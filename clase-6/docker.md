# Docker:

Vamos a ver como dockerizar nuestra aplicación.

Nos llevamos la aplicacion a ubuntu de wsl2, tengo una carpeta llamada mis-proyectos.

Dentro del proyecto de x-react en la raiz llamado
Dockerfile.

## Dockerfile:

Vamos a ejecutar estos comandos y con esto le diremos que haga una build de la aplicacion, que obtenga la salida y luego se la pase al servidor nginx, eso lo haremos con nuestro archivo Dockerfile:

```
FROM node:20-alpine as builder

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:stable-alpine

COPY --from=builder /app/build /usr/share/nginx/html

COPY --from=builder /app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "g", "daemon off;" ]
```

- Me bajo una imagen de node y creo un container: FROM node:20-alpine as builder

- Dentro del container me creo la aplicacion app: WORKDIR /app

- Copio el contenido de los package.json: COPY package\*.json ./

- Hago la instalación de todas las dependencias: RUN npm install

- Copio todo mi código: COPY . .

- Creo el ejecutable, obtenemos la carpeta build: RUN npm run build

- Me bajo el container de nginx: FROM nginx:stable-alpine

- Copio los archivo de la build a esa ruta en nginx: COPY --from=builder /app/build /usr/share/nginx/html

- Copio la configuracion de nginx, es un archivo de configuracion creado por nosotros: COPY --from=builder /app/nginx.conf /etc/nginx/conf.d/default.conf

- Expongo el puerto para nuestra app: EXPOSE 80

- Lanzo nuestra app: CMD ["nginx", "g", "daemon off;" ]

## nginx.conf:

```
server {
  listen 80;
  location / {
    root /usr/share/nginx/html;
    index index.html index.htm;
    try_files $uri $uri/ /index.html =404;
  }
}
```

Tambien debemos crear un archivo de .dockerignore en nuestro raiz del priyecto y colocamos esto:

## .dockerignore:

Practicamente podemos poner los que tenemos en nuestro .gitignore.

```
node_modules
build
npm-debug.log
```

Ahora dentro del poryecto por consola ejecutamos, para esto tenemos que tener instalado docker:

Vemos los comandos:

```bash
docker version
```

```bash
docker image ls
```

```bash
docker container ls
```

## Vamos a generar la imagen en docker:

```bash
docker build . -t x-react
```

## Para ver la imagen creada:

```bash
docker image ls
```

## Creamos un contenedor basado en esa imagen:

```bash
docker container run -d -p 80:80 x-react:latest
```

Al ejecutar nos retorna un id, un numero muy largo por consola, podemos comprobar si ya esta corriendo:

```bash
docker container ls
```

Ahora nos vamos localhost:80 tenemos nuestra app corriendo. Ahora nos vamos al navegador y ejecutamos el inspector vemos que quien nos sirve la aplicacion es nginx, lo vemos en la pestaña de network. Buscamos server.

Para parar la ejecucion:

```bash
docker container stop x-react:latest
```

Para volverlo arrancar:

```bash
docker container start x-react:latest
```

Para borrar el contenedor:

```bash
docker container -rm x-react:latest
```
