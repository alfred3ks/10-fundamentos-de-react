import React from 'react';

// Creamos el componente de clase:
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      info: null,
    };
  }
  componentDidCatch(error, info) {
    console.log('error:', { error });
    console.log('info:', { info });
    this.setState({ error, info });
  }

  render() {
    const { error, info } = this.state;
    if (error) {
      return (
        <div>
          <h2>Ooops! There was an error</h2>
          <div>
            <code>{error.message}</code>
          </div>
          <div>
            <code>{JSON.stringify(info)}</code>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
