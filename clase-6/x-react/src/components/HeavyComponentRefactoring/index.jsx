import PropTypes from 'prop-types';

// Funcion de fibonacci para ver memo:
const fibonacci = (n) => {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
};

// Componente que consume la serie de fibonacci:
const HeavyComponentRefactoring = ({ value }) => {
  const result = fibonacci(value);
  return (
    <div>
      Fibonacci({value}) = <code>{result}</code>
    </div>
  );
};

// PropTypes:
HeavyComponentRefactoring.propTypes = {
  value: PropTypes.number.isRequired,
};

export default HeavyComponentRefactoring;
