/* eslint-disable react/display-name */
import clsx from 'clsx';
import './TextArea.css';

import PropTypes from 'prop-types';
import { forwardRef } from 'react';

// const TextArea = forwardRef({ className, ...props }) => {
//   return (
//     <div className={clsx('textarea', className)}>
//       <textarea
//         className="textarea-input"
//         name="textarea"
//         id="textarea"
//         cols="30"
//         rows="10"
//         {...props}
//       ></textarea>
//     </div>
//   );
// };

// const TextArea = forwardRef({ className, innerRef, ...props }) => {
//   return (
//     <div className={clsx('textarea', className)}>
//       <textarea
//         className="textarea-input"
//         name="textarea"
//         id="textarea"
//         cols="30"
//         rows="10"
//         ref={innerRef}
//         {...props}
//       ></textarea>
//     </div>
//   );
// };

const TextArea = forwardRef(({ className, ...props }, ref) => {
  return (
    <div className={clsx('textarea', className)}>
      <textarea
        className="textarea-input"
        name="textarea"
        id="textarea"
        cols="30"
        rows="10"
        ref={ref}
        {...props}
      ></textarea>
    </div>
  );
});

// Creamos las propTypes:
TextArea.propTypes = {
  className: PropTypes.string,
  innerRef: PropTypes.object,
};

export default TextArea;
