import clsx from 'clsx';
import './FormField.css';

const FormField = ({ className, autofocus, label, ...props }) => {
  return (
    <div className={clsx('formField', className)}>
      <label htmlFor="form" className="formField-label">
        <span>{label}</span>
        <input
          className={'formField-input'}
          autoComplete="off"
          id="form"
          {...props}
        />
      </label>
    </div>
  );
};

export default FormField;
