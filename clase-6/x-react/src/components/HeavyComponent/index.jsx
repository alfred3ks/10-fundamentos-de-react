import PropTypes from 'prop-types';
import { memo, useMemo } from 'react';

// Funcion de fibonacci para ver memo:
const fibonacci = (n) => {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
};

// Componente que consume la serie de fibonacci:
export const HeavyComponent = ({ value }) => {
  const result = useMemo(() => {
    fibonacci(value);
  }, [value]);
  return (
    <div>
      Fibonacci({value}) = <code>{result}</code>
    </div>
  );
};

// Solucionamos problema de performance usando memo:
const MemoHeavyComponent = memo(HeavyComponent);
// const MemoHeavyComponent = memo(HeavyComponent, (prevProps, nextProps) => {
//   console.log(nextProps, prevProps);
//   return prevProps.value === nextProps.value;
// });

// PropTypes:
HeavyComponent.propTypes = {
  value: PropTypes.number.isRequired,
};

export default MemoHeavyComponent;
