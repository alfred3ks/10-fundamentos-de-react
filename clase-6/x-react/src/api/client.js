// Importamos la libreria
import axios from 'axios';

// Creamos el cliente de axios:
const client = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
});

// Creamos un interceptor de la respuesta para sacar el data y manejamos las respuestas de error de la api:
client.interceptors.response.use(
  (response) => response.data,
  (error) => {
    console.log(error);
    if (error.response) {
      // 400 / 500 server error: pro aqui errores del servidor un {}
      return Promise.reject({
        message: error.response.statusText,
        ...error.response,
        ...error.response.data,
      });
    }
    // Por aqui errores de network, Request error, sin conexion a internet: un {}
    return Promise.reject({ message: error.message });
  }
);

// Método para guardar el token en axios:
export const setAuthorizationHeader = (token) =>
  (client.defaults.headers.common['Authorization'] = `Bearer ${token}`);

// Método para quitar el token de axios:
export const removeAuthorizationHeader = () => {
  delete client.defaults.headers.common['Authorization'];
};

export default client;
