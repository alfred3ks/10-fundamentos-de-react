import { createContext, useContext } from 'react';

// creamos el contexto:
export const AuthContext = createContext(false);

// Creamos un custom hook:
export const useAuth = () => {
  const auth = useContext(AuthContext);
  return auth;
};
