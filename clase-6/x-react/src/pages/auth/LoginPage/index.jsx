import { useEffect, useRef, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Button from '../../../components/shared/Button';
import { login } from '../service';
import { useAuth } from '../context';
import FormField from '../../../components/shared/FormField';

// Importamos de react dom portal:
import { createPortal } from 'react-dom';

import './LoginPage.css';
import copyStyles from './copyStyles';

const LoginPage = () => {
  // Recibimos del contexto onLogin con el custom hook:
  const { onLogin } = useAuth();

  // Estado unificado para los imputs:
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });

  // Creamos el estado para el error:
  const [error, setError] = useState(null);

  // Creamos el estado para conexiones lentas:
  const [isFeching, setIsFeching] = useState(false);

  // accedemos al objeto location de react router:
  const location = useLocation();
  // Usamos el hook para navegar:
  const navigate = useNavigate();

  // Funcion handle del formulario
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
    try {
      setIsFeching(true);
      await login(credentials);
      setIsFeching(false);
      // aqui el usuario ya esta logueado: Lanzamos el evento que viene del padre:
      onLogin();

      // Si tenemos algo en location redireccionamos o vamos al home:
      const to = location?.state?.from?.pathname || '/';
      navigate(to, { replace: true });
    } catch (error) {
      setIsFeching(false);
      setError(error);
      // Establecer un temporizador para limpiar el error después de 5 segundos
      setTimeout(() => {
        setError(null);
      }, 5000);
    }
  };

  // Creamos la funcion para los inputs controlados:
  const handleChange = (e) => {
    setCredentials((currentCredentials) => ({
      ...currentCredentials,
      [e.target.name]: e.target.value,
    }));
  };

  // Hacemos destructuring de las credenciales:
  const { username, password } = credentials;
  // Creamos la variable para el boton: esta linea de ejecuta en cada render
  const disabled = !(username && password) || isFeching;

  return (
    <div className="loginPage">
      <h1 className="loginPage-title">Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <FormField
          type="text"
          name="username"
          label={'phone, email or username'}
          className={'loginForm-field'}
          autofocus
          id="username"
          onChange={handleChange}
          value={username}
        />
        <FormField
          type="password"
          name="password"
          label={'password'}
          className={'loginForm-field'}
          id="password"
          onChange={handleChange}
          value={password}
        />
        {/* Mostramos el boton de cargando al hacer Login con estado */}
        <Button
          className="loginForm-submit"
          type="submit"
          $variant="primary"
          disabled={disabled}
        >
          {!isFeching ? 'Login' : 'Loading...'}
        </Button>
        {/* Mostramos error */}
        {error && <div className="loginPage-error">{error.message}</div>}
      </form>
    </div>
  );
};

// Portal:
const LoginPagePortal = () => {
  const portalContainer = useRef(document.createElement('div'));
  useEffect(() => {
    const externalWindow = window.open(
      '',
      '',
      'width=600 height=400 left=500 top=200'
    );

    copyStyles(document, externalWindow.document);

    externalWindow.document.body.appendChild(portalContainer.current);
    () => {
      externalWindow.close();
    };
  });
  return createPortal(<LoginPage />, portalContainer.current);
};

// export default LoginPage;
export default LoginPagePortal;
