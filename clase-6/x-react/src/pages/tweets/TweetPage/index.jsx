import { useEffect, useState } from 'react';
import Content from '../../../components/layout/Content';
import { useParams, useNavigate } from 'react-router-dom';

import { getTweet } from '../service';

const TweetPage = () => {
  // Guardamos la respuesta en un estado:
  const [tweet, setTweet] = useState(null);
  const navigate = useNavigate();
  // const params = useParams();

  const { id } = useParams();
  // console.log(id);

  // Hacemos la peticion al servidor:
  useEffect(() => {
    getTweet(id)
      .then((tweet) => setTweet(tweet))
      .catch((error) => {
        if (error.status === 404) {
          console.log('entro');
          navigate('/404');
        }
      });
  }, [navigate, id]);

  return (
    <Content title="Tweet detail">
      <div>
        Tweet detail {id} goes here...
        <div>{tweet && <code>{JSON.stringify(tweet)}</code>}</div>
      </div>
    </Content>
  );
};

export default TweetPage;
