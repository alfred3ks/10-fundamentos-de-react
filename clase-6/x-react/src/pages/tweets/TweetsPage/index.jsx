// Importamos estilos usando modules css:
// import styles from './TweetsPage.module.css';
// console.log(styles);

// Importamos la llamada a la api:
import { getLatestTweets } from '../service';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
// Importamos el boton:
import Button from '../../../components/shared/Button';

import Tweet from '../Tweet';
import Content from '../../../components/layout/Content';

import './TweetsPage.css';

const EmptyList = () => (
  <div style={{ textAlign: 'center' }}>
    <p>Be the first one!</p>
    <Button $variant="primary">Create tweet</Button>
  </div>
);

// Componente TweetPage:
const TweetsPage = ({ dark }) => {
  // declaramos un state para los tweets:
  const [tweets, setTweets] = useState([]);

  // Llamamos al metodo para llamar a la api: usamos useEffect: Promesas:
  // useEffect(() => {
  //   getLatestTweets().then((tweets) => {
  //     setTweets(tweets);
  //   });
  // }, []);

  // Llamamos al metodo para llamar a la api: usamos useEffect: Async await:
  useEffect(() => {
    const fetchTweets = async () => {
      const tweets = await getLatestTweets();
      setTweets(tweets);
      // Error boundary provocarlo:
      // setTweets({ length: 5 });
    };

    fetchTweets();
  }, []);

  // Ejemplo de useEffect con dependencia:
  // useEffect(() => {
  //   document.title = dark ? 'dark' : 'light';

  //   // Funcion de limpieza: aqui no tiene mucho sentido pero es asi:
  //   return () => {
  //     console.log('Exit, función de limpieza.');
  //   };
  // }, [dark]);

  // Transformamos el cero del arreglo a booleano:
  const hasTweets = !!tweets.length;
  // console.log(hasTweets);

  return (
    <Content title="What's going on...">
      <div className={'tweetsPage'}>
        {/* Metemos un condicional para mostrar tweets o mensaje */}
        {hasTweets ? (
          <ul>
            {tweets?.map(({ id, ...tweet }) => (
              <li key={id}>
                {/* {Ruta absoluta} */}
                {/* <Link to={`/tweets/${id}`}> */}
                {/* Ruta relativa */}
                <Link to={`${id}`}>
                  <Tweet {...tweet} />
                </Link>
              </li>
            ))}
          </ul>
        ) : (
          <EmptyList />
        )}
      </div>
    </Content>
  );
};

export default TweetsPage;
