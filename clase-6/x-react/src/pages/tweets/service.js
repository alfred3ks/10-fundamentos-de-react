// importamos el cliente:
import client from '../../api/client';

// definimos el end point de la api:
const tweetsUrl = '/api/tweets';

// definimos el metodo para hacer la peticion get a la api:
export const getLatestTweets = () => {
  // Hacemos la consulta a la api expandida:
  const url = `${tweetsUrl}?_expand=user&_embed=likes&_sort=updatedAt&_order=desc`;
  return client.get(url);
};

// Método para solicitar tweet página detalle:
export const getTweet = (tweetId) => {
  const url = `${tweetsUrl}/${tweetId}`;
  // Hacemos una peticion:
  return client.get(url);
};

// Método para crear un nuevo tweet:
export const createTweet = (tweet) => {
  const url = tweetsUrl;
  return client.post(url, tweet);
};
