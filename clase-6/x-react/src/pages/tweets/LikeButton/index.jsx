import clsx from 'clsx';
import IconLiked from '../../../components/shared/IconLiked';
import IconNotLiked from '../../../components/shared/IconNotLiked';

// importamos propTypes:
import PropTypes from 'prop-types';

import './LikeButton.css';

const LikeButton = ({ likes, isLike, onLike }) => {
  const Icon = isLike ? IconLiked : IconNotLiked;

  return (
    <div
      className={clsx('likeButton', {
        'likeButton--active': isLike,
      })}
      onClick={(event) => {
        event.preventDefault();
        onLike(event);
      }}
    >
      <span className="likeButton-icon">
        <Icon width="20" height="20" />
      </span>
      <span className="likeButton-label">{likes}</span>
    </div>
  );
};

// Declaramos las propTypes:
LikeButton.propTypes = {
  likes: PropTypes.number.isRequired,
  isLike: PropTypes.bool,
  onLike: PropTypes.func.isRequired,
};

// Declaramos los valores por defecto
LikeButton.defaultProps = {
  isLike: false,
};

export default LikeButton;
