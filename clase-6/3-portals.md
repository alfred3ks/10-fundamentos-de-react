# Portals:

En React, un Portal es una característica que te permite renderizar un subárbol de componentes en un nodo DOM que está fuera del árbol de componentes principal de tu aplicación. Esto significa que puedes renderizar contenido en cualquier parte del DOM, independientemente de la jerarquía de tus componentes React.

Los Portales son útiles en situaciones donde necesitas renderizar contenido en un nodo DOM que no es hijo directo de tu componente principal, como al crear modales, popovers, menús desplegables u otras superposiciones.

https://react.dev/reference/react-dom/createPortal

Vamos a hacer un portals del LoginPage.jsx: Lo que haremos es renderizar este componente fuera de donde seria su sitio natural dentro del arbol de html del DOM. desde el punto de vista de react el sigue en su sitio donde esta creado pero a nivel del DOM estara fuera dentro de otra ventana flotante.

Para esto tenemos la funcion createPortal(). Nos permite renderizar un elemento de react en un lugar distinto del DOM. Lo que haremos en el HTML nos vamos a crear un div nuevo donde vamos a renderizar el elemento que queremos hacer portal, en este caso en componente de LoginPage.jsx.

## index.html:

```html
<div id="portal"></div>
```

Vamos a realizar unas modificaciones a LoginPage.jsx: Vamos a hacer un wrap del componente LoginPage.jsx dentro del otro componente que llamaremos LoginPagePortal deltro del componente LoginPage.jsx:

## LoginPage.jsx:

```javascript
// Portal:
const LoginPagePortal = () => {
  return createPortal(<LoginPage />, document.getElementById('portal'));
};

export default LoginPagePortal;
```

Recargamos y vemos por el inspector que el login page ya no carga en el div de root sino en el div portal:

![](./img/portal.png)

Sin embargo desde el punto de vista de react sigue en su sitio:

![](./img/portal2.png)

Simplemente esta fuera solo en el DOM.

Ahora vamos a ver como sacar el LoginPage.jsx en otra ventana. Para esto nos ayudaremos de useRef para crearnos un div llamado portalContainer.

## LoginPage.jsx:

```javascript
// Portal:
// Portal:
const LoginPagePortal = () => {
  const portalContainer = useRef(document.createElement('div'));
  useEffect(() => {
    const externalWindow = window.open(
      '',
      '',
      'width=600 height=400 left=500 top=200'
    );

    externalWindow.document.body.appendChild(portalContainer.current);
    () => {
      externalWindow.close();
    };
  });
  return createPortal(<LoginPage />, portalContainer.current);
};
```

Ya vemos como se carga una nueva ventana pero sin estilos. El tema de estilo investigar por internet.
