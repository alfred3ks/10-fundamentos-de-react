# Error Boundary: Errores de render.

En React, un Error Boundary (o límite de error) es un componente de React especial que actúa como un contenedor para capturar y manejar errores que ocurren durante la renderización, en la vida útil del componente hijo o durante la actualización del árbol de componentes.

Los Error Boundaries son útiles porque ayudan a prevenir que los errores de un componente se propaguen por toda la aplicación, lo que podría dejar la interfaz de usuario en un estado no deseado o incluso hacer que la aplicación se bloquee.

Cuando un error ocurre dentro de un componente que está envuelto por un Error Boundary, React desactiva el estado y las actualizaciones de los componentes descendientes en lugar de lanzar el error al navegador. Esto permite que la aplicación permanezca en un estado manejable y evita que los errores afecten negativamente la experiencia del usuario.

Esto es algo que aun queda de los componentes de clase. Para esto hay que usar clases. No se puede hacer un error boundary con hook y funciones. Vamos a hacer un ejemplo.

Lo que vamos a hacer es envolver nuestra aplicación por medio de un componente de error boundary por si se produce un error de render poderlo capturar y manejar. Lo haremos en main.jsx.

Nos creamos en components una carpeta llamada errors. dentro nos creamos nuestro componente de ErrorBoundary.jsx:

## ErrorBoundary.jsx:

```javascript
import React from 'react';

// Creamos el componente de clase:
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      info: null,
    };
  }
  componentDidCatch(error, info) {
    console.log('error:', { error });
    console.log('info:', { info });
    this.setState({ error, info });
  }

  render() {
    const { error, info } = this.state;
    if (error) {
      return (
        <div>
          <h2>Ooops! There was an error</h2>
          <div>
            <code>{error.message}</code>
          </div>
          <div>
            <code>{JSON.stringify(info)}</code>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
```

Lo colocamos a nivel del main.jsx:

## main.jsx:

```javascript
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ErrorBoundary>
      <BrowserRouter>
        <AuthContextProvider initiallyLogged={isUserLogged}>
          <App />
        </AuthContextProvider>
      </BrowserRouter>
    </ErrorBoundary>
  </React.StrictMode>
);
```

Ahora cualquier error de render nuestro componente lo va a capturar. Ahora para provocar un error para ver como actual. Vamos al componente TweetsPage.jsx y cambiamos:

## TweetsPage.jsx:

```javascript
// Llamamos al metodo para llamar a la api: usamos useEffect: Async await:
useEffect(() => {
  const fetchTweets = async () => {
    const tweets = await getLatestTweets();
    // setTweets(tweets);
    // Error boundary provocarlo:
    setTweets({ length: 5 });
  };

  fetchTweets();
}, []);
```

Y vemos como si lo captura.

![](./img/boundary.png)
