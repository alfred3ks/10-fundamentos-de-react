# Lazy loading y code splitting:

Lazy permite retrasar la carga del código de un componente hasta que es realmente necesario.

Cuando cargamos un componente con lazy el código del componente es seaparado del bundle principal. Con el componente Suspense podemos definir una interfaz alternativa que sera presentada mientras descarga el código del componente (Loader, spinner).

React nos sofrece la manera de retrazar carga de código desde el browser. Lo que vemos en nuestra aplicación es que se carga en el navegador una pagina html que tiene un div que es donde de inyecta la aplicacion por medio del código de javascript, ese código js viene en un bundle.js el cual es un solo archivo, lo que buscamos es partir en partes ese archivo para solo usar el que requerimos en el momento que se pida. Esta tecnica consiste en hacer particiones de nuestro código que solo se usaran si son requeridas. Esta es la tecnica de code splitting. Recordar este bundle.js se genera al hacer un build de la aplicacion para ir a produccion.

React nos entrega dos funciones, una lazy para el retraso del código y el componente Suspense.

Vamos a verlo con el componente loginPage.jsx lo vemos en App.jsx:

## App.jsx:

Cargamos el componente en modo Lazy y envolvemos nuestro componente por medio del componente <Suspense></Suspense> y le pasamos el atributo fallback={} con un componente que puede ser un Loader o Spinner.

```javascript
import { Suspense, lazy } from 'react';
// Hacemos el import usando la función lazy()
const LoginPage = lazy(() => import('./pages/auth/LoginPage'));

<Route
  path="/login"
  element={
    <Suspense fallback={<div>Loading...</div>}>
      <LoginPage />
    </Suspense>
  }
/>;
```

La idea es mostrar algo miestras se produce la carga de esa pagina. En el login no se llega a ver. Se ve pero muy rapido.

NOTA: En CRA es cuando se ve el bundle.js cosa que en vite no se ve.

El componente <Suspense></Suspense> en react nacio para ser usado con lazy(), pero muchas librerias lo suelen usar para tratar promesas que se producen dentro del componente que envuelven.
