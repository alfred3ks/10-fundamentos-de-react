# Agregar estilos al login con portals:

Para poder agregar estilos al login y que cargue con esos estilo hemos agregado una funcion llamada copyStyles donde podemos hacer la carga de estilos a esa pantalla externa y el código lo vemos a continuación:

## copyStyles.js:

```javascript
export default function copyStyles(sourceDoc, targetDoc) {
  Array.from(sourceDoc.styleSheets).forEach((styleSheet) => {
    if (styleSheet.cssRules) {
      // for <style> elements
      const newStyleEl = sourceDoc.createElement('style');

      Array.from(styleSheet.cssRules).forEach((cssRule) => {
        // write the text of each rule into the body of the style element
        newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText));
      });

      targetDoc.head.appendChild(newStyleEl);
    } else if (styleSheet.href) {
      // for <link> elements loading CSS from a URL
      const newLinkEl = sourceDoc.createElement('link');

      newLinkEl.rel = 'stylesheet';
      newLinkEl.href = styleSheet.href;
      targetDoc.head.appendChild(newLinkEl);
    }
  });
}
```

Este código lo cargamos en nuestro componente loginPage.jsx:

## LoginPage.jsx:

```javascript
// Portal:
const LoginPagePortal = () => {
  const portalContainer = useRef(document.createElement('div'));
  useEffect(() => {
    const externalWindow = window.open(
      '',
      '',
      'width=600 height=400 left=500 top=200'
    );

    copyStyles(document, externalWindow.document);

    externalWindow.document.body.appendChild(portalContainer.current);
    () => {
      externalWindow.close();
    };
  });
  return createPortal(<LoginPage />, portalContainer.current);
};

// export default LoginPage;
export default LoginPagePortal;
```

Vemos que llamamos la funcion copyStyles:

```javascript
copyStyles(document, externalWindow.document);
```

Ahora cuando ejecutamos nuestro login vemos como tiene aplicados los estilos.
