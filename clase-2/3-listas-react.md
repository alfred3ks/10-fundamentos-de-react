# Listas en react:

Vamos a crear un listado de Tweets. Lo que haremos es renderizar un listado harcodeado en nuestro componente.

La estructura que haremos sera que crearemos una carpeta llamada pages, sera donde iremos colocando nuestras páginas. Creamos nuestra carpeta para los tweets.

Dentro de esta carpeta creamos el componente TweetsPage.jsx:

## TweetsPage.jsx:

```javascript
// Arreglo de Tweets:
const tweets = [
  {
    content:
      "'Soy muy fan tuya, pero ahora no me acuerdo cómo te llamas' (Una desconocida, en la calle).",
    userId: 1,
    updatedAt: '2021-03-15T18:23:57.579Z',
    id: 1,
  },
  {
    content:
      "'Soy muy fan tuya, pero ahora no me acuerdo cómo te llamas' (Una desconocida, en la calle).",
    userId: 1,
    updatedAt: '2021-03-15T18:24:56.773Z',
    id: 2,
  },
];

const TweetsPage = () => {
  return (
    <div>
      <ul>
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};

export default TweetsPage;
```

Bastante sencillo, lo que vemos es que al renderizar listas debemos unas un atributo key={}, muy necesario que nos exige React. Cada elemento de una lista debe tener una key unica. Tambien vemos que para iterar el arreglo usamos el método map().

Lo importamos en nuestro componente <App/>:

## App.jsx:

```javascript
import TweetsPage from './pages/tweets/TweetsPage';

const App = () => {
  return <TweetsPage />;
};

export default App;
```
