# CSS en React:

Vamos a ver como podemos implementar CSS en React. Existen varias formas de implementar css en react.

- Atributo className, funciona igual que el atributo class. Tambien podemos usar cualquier framework de CSS como tailwind o bootstrap.
- Estilos en linea con el atributo style pero las propiedades las tenemos que pasar en camelCase.
- Librerias CSS in JS, tenemos tales como styled component, Emotion. Hay varias formas.

Vamos a aplicar estilos al componente TweetsPage.jsx:

## 🎯 Mediante el atributo className:

Creamos nuestra hoja de estilo, normalmente la llamamos como el propio componente, para nuestro caso TweetsPage.css

## TweetsPage.css:

```css
.tweetsPage {
  font-size: 16px;
  font-weight: bold;
}
```

## TweetsPage.jsx:

```javascript
// Importamos los estilos:
import './TweetsPage.css';

<div className="tweetsPage">
  <ul>
    {tweets.map((tweet) => {
      return <li key={tweet.id}>{tweet.content}</li>;
    })}
  </ul>
</div>;
```

## Estilos condicionales:

Ya con esto lo tenemos. Aunque como tenemos acceso a js podemos meter expresiones en el classname: podriamos usar condicionales que si tenemos una clase se aplicara un estilo sino otro:

## TweetsPage.css:

```css
.tweetsPage {
  font-size: 16px;
  font-weight: bold;
}

.dark {
  color: white;
  background-color: black;
}

.light {
  color: crimson;
  background-color: white;
}
```

## TweetsPage.jsx:

```javascript
const dark = true;

const TweetsPage = () => {
  return (
    <div className={`tweetsPage ${dark ? 'dark' : 'light'} `}>
      <ul>
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};
```

Como vemos asi podemos aplicar estilos de manera dinamica. Tambien podriamos ver que esa variable dinamica viene por props en vez de estar declarada como hemos visto:

## TweetsPage.jsx:

```javascript
const TweetsPage = ({ dark }) => {
  return (
    <div className={`tweetsPage ${dark ? 'dark' : 'light'}`}>
      <ul>
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};
```

## App.jsx:

```javascript
const App = () => {
  return <TweetsPage dark={false} />;
};
```

## Librerias de css:

Esto de pasar las clases de manera dinámica se podria complicar algo si se llega a dar el caso podemos usar tambien alguna libreria como estas que nombramos a continuación:

- classname de JedWatson:

https://github.com/JedWatson/classnames

- clsx de lukeed:

https://github.com/lukeed/clsx

Con estas librerias nos permiten construir esas clases pasandas de manera dinamica al aributi className de una manera mas facil. Vamos a probar clsx, la debemos instalar.

```bash
npm i clsx
```

## TweetsPage.jsx:

```javascript
// importamos la libreria clsx:
import clsx from 'clsx';

const TweetsPage = ({ dark }) => {
  const classClsx = clsx('tweetsPage', { dark: dark, light: !dark });

  return (
    <div className={classClsx}>
      <ul>
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};
```

## app.jsx:

```javascript
const App = () => {
  return <TweetsPage dark />;
};

export default App;
```

Cuando usamos el atributo className podemos en algun momento tener problemas de estilos que pisan a otros. Para solucionar esto aparecieron los modulos de css.

## Modulos de css:

Por cada fichero css que generemos se generaran clases unicas por medio de un hash, asi ya no tendremos colisiones de estilos. Para usar modules de css tenemos que nombrar nuestro archivo asi:

TweetsPage.module.css

En nuestro componente lo importamos:

## TweetsPage.jsx:

```javascript
// Importamos estilos usando modules css:
import styles from './TweetsPage.module.css';

const TweetsPage = ({ dark }) => {
  const classClsx = clsx(styles.TweetsPage, {
    [styles.dark]: dark,
    [styles.light]: !dark,
  });

  return (
    <div className={classClsx}>
      <ul>
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};
```

## App.jsx:

```javascript
const App = () => {
  return <TweetsPage dark />;
};

export default App;
```

En el import del modulo de css el styles es un objeto {}, dentro de ese objeto tenemos las clases definidas con el hash unico, lo podemos ver por consola:

```javascript
{tweetsPage: '_tweetsPage_b78bf_1', dark: '_dark_b78bf_11', light: '_light_b78bf_21'}
```

Vemos el nombre nombre de la clase con el hash unico.

## Aplicando estilos en linea:

Esto no es muy usado pero para aplicar estilos rapido nos vendria bien.

## ## TweetsPage.jsx:

```javascript
const TweetsPage = ({ dark }) => {
  const classClsx = clsx(styles.TweetsPage, {
    [styles.dark]: dark,
    [styles.light]: !dark,
  });

  return (
    <div className={classClsx}>
      <ul style={{ listStyle: 'none', padding: '3', border: '1px solid blue' }}>
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};
```

Mediante el atributo style={}, le pasamos un objeto con las propiedades, { listStyle: 'none', padding: '3', border: '1px solid blue' }.

## Librerias externas, styled components:

Podemos ver la documentación de style component:

https://styled-components.com/

Vamos a instalar la libreria en nuestro proyecto:

```bash
npm install styled-components
```

Lo que haremos con esta libreria es crear componentes con determinados estilos.

Vamos a crearnos un componente button, para eso nos creamos una carpeta llamada component.

## Button.jsx:

```javascript
// Importamos la libreria styled component:
import styled from 'styled-components';

const accentColor = 'rgb(29, 161, 242)';

// Creamos el componente button:
const Button = styled.button`
  cursor: pointer;
  border-radius: 9999px;
  border-style: solid;
  border-width: 1px;
  background-color: ${(props) =>
    props.$variant === 'primary' ? accentColor : 'white'};
  border-color: ${accentColor};
  color: ${(props) => (props.$variant === 'primary' ? 'white' : accentColor)};
  display: inline-flex;
  align-items: center;
  font: inherit;
  font-weight: bold;
  min-height: 36px;
  justify-content: center;
  min-width: 72px;
  outline-style: none;
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
  padding: 0 30px;
  pointer-events: ${(props) => (props.disabled ? 'none' : 'auto')};
  text-decoration: none;
  transition: background-color 0.2s;
  &:hover {
    background-color: ${(props) =>
      props.$variant === 'primary'
        ? 'rgb(26, 145, 218)'
        : 'rgba(29, 161, 242, 0.1)'};
  }
`;

export default Button;
```

## App.jsx:

```javascript
import Button from './components/Button';
import TweetsPage from './pages/tweets/TweetsPage';

const App = () => {
  return (
    <div>
      <TweetsPage dark={false} />
      <Button $variant={'primary'}>Click me!!</Button>
    </div>
  );
};

export default App;
```

Esto es hacer css en javascript.
