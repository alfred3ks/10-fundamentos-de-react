import { useState } from 'react';
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';

const App = () => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(false);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);
  return (
    <div>
      {isLogged ? (
        <TweetsPage dark={false} />
      ) : (
        <LoginPage onLogin={handleLogin} />
      )}
    </div>
  );
};

export default App;
