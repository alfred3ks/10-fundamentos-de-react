// Importamos la libreria
import axios from 'axios';

// Creamos el cliente de axios:
const client = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
});

// Creamos un interceptor de la respuesta para sacar el data:
client.interceptors.response.use((response) => {
  return response.data;
});

// Método para guardar el token:
export const setAuthorizationHeader = (token) =>
  (client.defaults.headers.common['Authorization'] = `Bearer ${token}`);

export default client;
