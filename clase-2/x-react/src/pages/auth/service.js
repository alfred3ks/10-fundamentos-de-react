import client, { setAuthorizationHeader } from '../../api/client';

export const login = (credentials) => {
  // Usamos el cliente se axios con el metodo post a la url pasandole las credenciales:
  return client
    .post('/auth/login', credentials)
    .then(({ accessToken }) => setAuthorizationHeader(accessToken));
};

// Async await:
// export const login = async (credentials) => {
//   try {
//     // Utilizamos el cliente de Axios con el método post en la URL pasándole las credenciales
//     const response = await client.post('/auth/login', credentials);
//     console.log(response);
//     // Puedes devolver la respuesta si es necesario
//     return response;
//   } catch (error) {
//     // Manejar errores aquí
//     console.error('Error al iniciar sesión:', error);
//     // Puedes lanzar el error nuevamente si necesitas que sea manejado en otra parte del código
//     throw error;
//   }
// };
