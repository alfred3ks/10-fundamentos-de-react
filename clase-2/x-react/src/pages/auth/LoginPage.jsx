import Button from '../../components/Button';
import { login } from './service';

const LoginPage = ({ onLogin }) => {
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // accedemos a los input:
    const username = e.target.username.value;
    const password = e.target.password.value;

    // Usamos el metodo del servicio, le pasamos los datos {}:
    await login({ username, password });

    // aqui el usuario ya esta logueado: Lanzamos el evento o método que viene del padre:
    onLogin();
  };

  return (
    <div>
      <h1>Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" name="username" id="username" />
        <input type="password" name="password" id="password" />
        <Button type="submit" $variant="primary">
          Log in
        </Button>
      </form>
    </div>
  );
};

export default LoginPage;
