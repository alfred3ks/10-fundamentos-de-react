// Importamos los estilos:
// import './TweetsPage.css';

// Importamos estilos usando modules css:
import styles from './TweetsPage.module.css';
// console.log(styles);

// importamos la libreria clsx:
import clsx from 'clsx';

// Importamos la llamada a la api:
import { getLatestTweets } from './service';
import { useEffect, useState } from 'react';

const TweetsPage = ({ dark }) => {
  // declaramos un state para los tweets:
  const [tweets, setTweets] = useState([]);

  const classClsx = clsx(styles.TweetsPage, {
    [styles.dark]: dark,
    [styles.light]: !dark,
  });

  // Llamamos al metodo para llamar a la api: usamos useEffect: Promesas:
  // useEffect(() => {
  //   getLatestTweets().then((tweets) => {
  //     setTweets(tweets);
  //   });
  // }, []);

  // Llamamos al metodo para llamar a la api: usamos useEffect: Async await:
  useEffect(() => {
    const fetchTweets = async () => {
      const tweets = await getLatestTweets();
      setTweets(tweets);
    };

    fetchTweets();
  }, []);

  // Ejemplo de useEffect con dependencia:
  useEffect(() => {
    document.title = dark ? 'dark' : 'light';

    // Funcion de limpieza: aqui no tiene mucho sentido pero es asi:
    return () => {
      console.log('Exit');
    };
  }, [dark]);

  return (
    <div className={classClsx}>
      <ul
        style={{ listStyle: 'none', padding: '30', border: '1px solid blue' }}
      >
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};

export default TweetsPage;
