// importamos el cliente:
import client from '../../api/client';

// definimos el end point de la api:
const tweetsUrl = '/api/tweets';

// definimos el metodo para hacer la peticion get a la api:
export const getLatestTweets = () => {
  return client.get(tweetsUrl);
};
