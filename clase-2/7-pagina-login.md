# Página de Login:

Empezamos a realizar la página del login, la estructura que vamos a usar es la siguiente, dentro de la carpeta page nos vamos a crear una llamada auth. Dentro de esta un fichero llamado LoginPage.jsx para crear nuestro componente de login.

En este componente vamos a empezar a ver el tema de formularios. Este componente va a tener su propio service que llamaremos service.js para podernos conectar con la api.

Lo que haremos es intentar simular un login. Lo que haremos es que si el usuario esta loguedo muestre el listado de tweets y si no mostrar el formulario de login.

OJO nos creamos en sparrest un usuario:

![](../doc/sparest-subir-un-usuario.png)

Los datos son los siguientes:

luke@kc.com
121212

Para el formulario de inputs no controlados para el envio de la informacion tenemos el evento onSubmit={} que recibe una funcion handleSubmit. Esta funcion recibe el evento, y podemos prevenir con preventDefault() el envio por defecto del formulario. Tambien vemos que podemos obtener los datos del input con el evento.

## LoginPage.jsx:

```javascript
import Button from '../../components/Button';
import { login } from './service';

const LoginPage = () => {
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // accedemos a los input:
    const username = e.target.username.value;
    const password = e.target.password.value;

    // Usamos el metodo del servicio, le pasamos los datos {}:
    await login({ username, password });
  };

  return (
    <div>
      <h1>Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" name="username" id="username" />
        <input type="password" name="password" id="password" />
        <Button type="submit" $variant="primary">
          Log in
        </Button>
      </form>
    </div>
  );
};

export default LoginPage;
```

En el objeto response cuando pasamos unas credenciales correctas la api nos retorna un JWT, Un Json Web Token. Un accesToken, tambien el codigo de respuesta es un 201. Lo vemos en la consola del navegador. Este accest token lo vamos a necesitar para peticiones futuras. El token lo vamos a guardar y en cada futura peticion este viajara en las cabeceras de autorizacion. Para esto axios nos ayuda mucho. Vamos a crearnos un metodo en api/cliente.js:

## pages/auth/service.js:

```javascript
import client from '../../api/client';

export const login = (credentials) => {
  // Usamos el cliente se axios con el metodo post a la url pasandole las credenciales:
  return client
    .post('/auth/login', credentials)
    .then((response) => console.log(response));
};
```

Vemos la salida por consola del objeto response: Si el usuario existe:

```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoibHVrZUBrYy5jb20iLCJpYXQiOjE3MDU4ODc3MDksImV4cCI6MTcwNTk3NDEwOX0._0X9fimwxSv55Hd9wRKyDeef9uhLaX0c7Gz0OHlb_KI"
}
```

Ahora en el fichero cliente.js de la api vamos a crear el metodo para guardar en las cabeceras de axios el token:

## api/cliente.js: Metodo para guardar el token:

```javascript
// Método para guardar el token:
export const setAuthorizationHeader = (token) =>
  (client.defaults.headers.common['Authorization'] = `Bearer ${token}`);
```

Cuando hacemos el login pasamos el token al las cabeceras de axios usando el método anterior:

## pages/auth/service.js: Le pasamos el método para guardar el token en axios.

```javascript
import client, { setAuthorizationHeader } from '../../api/client';

export const login = (credentials) => {
  // Usamos el cliente se axios con el metodo post a la url pasandole las credenciales:
  return client
    .post('/auth/login', credentials)
    .then(({ accessToken }) => setAuthorizationHeader(accessToken));
};
```

## Compartiendo estado:

Vamos a ver un concepto muy visto en react que es compartir estado, el componente <App/> es el encargado de mostrar o el login o el listado, si no estas logueado mostrara el login al loguearse mostrara el listado, ahora, el componente <LoginPage/> es el que sabe que el usuario esta logueado al ejecutarse la funcion login(). Necesitamos comunicarnos entre <App/> y <LoginPage/>. Necesitamos de alguna manera decirle a <App/> que el usuario esta logueado. Hemos visto que de un componente padre podemos pasar propiedades. Las props. Crearemos el estado el <App/> y pasaremos por props la informacion para cambiar el estado de <App/> y mostrar una cosa u la otra. Hemos tenido que subir el estado de <LoginPage/> a <App/>. Esto es lo que se conoce como elevar el estado.

## App.jsx:

```javascript
import { useState } from 'react';
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';

const App = () => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(false);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);
  return (
    <div>
      {isLogged ? (
        <TweetsPage dark={false} />
      ) : (
        <LoginPage onLogin={handleLogin} />
      )}
    </div>
  );
};

export default App;
```

Y el componente LoginPage.jsx:

## LoginPage.jsx:

```javascript
import Button from '../../components/Button';
import { login } from './service';

const LoginPage = ({ onLogin }) => {
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // accedemos a los input:
    const username = e.target.username.value;
    const password = e.target.password.value;

    // Usamos el metodo del servicio, le pasamos los datos {}:
    await login({ username, password });

    // aqui el usuario ya esta logueado: Lanzamos el evento o método que viene del padre:
    onLogin();
  };

  return (
    <div>
      <h1>Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" name="username" id="username" />
        <input type="password" name="password" id="password" />
        <Button type="submit" $variant="primary">
          Log in
        </Button>
      </form>
    </div>
  );
};

export default LoginPage;
```

Lo probamos y vemos que funciona. Ahora vemos que refrescamos la pagina y se pierde la informacion. No tenemos persistencia de datos. Otra cosa que hemos visto es como manejar un formulario pero de manera incontrolada. Tampoco hemos manejado el error si el usuario es incorrecto.

Tambien es bueno saber que cuando App.js se renderiza todos sus hijos tambien lo hacen. Cada vez que cambia su estado el se renderiza de nuevo y por consiguiente sus hijos tambien. Esto tenerlo claro. El DOM Virtual se renderiza luego react DOm se encarga de renderizar solo lo que ha cambiado en el DON del navegador.
