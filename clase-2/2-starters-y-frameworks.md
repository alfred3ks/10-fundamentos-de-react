# Starters y Frameworks:

Podemos crear nuestra aplicación de React desde cero, pero esto implica manejo de varias herramientas, bundler(webpack), traspilador (babel), linter (eslint), etc.

Lo mejor es usar alguna de estas herramientas:

- Aplicaciones SPA (Single page application):

  - CRA: Create React App: https://create-react-app.dev/ (DEPRECADA)
  - Vite: Vite JS React: https://vitejs.dev/

- Aplicaciones universales (Cliente Servidor, MPA): Frameworks
  - NextJS: https://nextjs.org/
  - Remix: https://remix.run/
  - Gatsby: https://www.gatsbyjs.com/

Aqui vamos a realizar una app con CRA. Yo usare Vite.js.

Vamos a crear la aplicación que vamos a desarrollar, un clon de twitter:

```bash
npm create vite@latest
```

La aplicación la llamaremos x-react. Este nos generara un proyecto inicial, como un esqueleto para poder iniciar nuestra aplicación.

Aqui tenemos la documentación de vite:

https://vitejs.dev/guide/

El punto de entrada de nuestra aplicación es main.jsx donde renderiza un componente <App/>. Esta es nuestra aplicación. Vemos que el componente <App/> esta envuelto por un componente <React.StrictMode><App/></React.StrictMode>, este es un componente de react para desarrollo, nos mostrara warnings si hacemos usamos metodos deprecados, etc. Esto solo lo tendremos en dearrollo, es producción no existe. Por eso a veces vemos que por consola una instruccion saca 2 salidas, es debido al modo estricto de la libreria que produce dos render.
