# 📌📌 Clonamos Sparrest:

Vamos a clonarnos el repositorio de sparrest.js: este sera la api que vamos a trabajar para trabajar en las clases que viene a continuacion:

https://github.com/kasappeal/sparrest.js

```bash
git clone https://github.com/kasappeal/sparrest.js
```

Entramos dentro del proyecto:

```bash
cd sparrest.js
```

Instalamos las depedencias:

```bash
npm install
```

Ahora cuando levantemos el proyecto sparrest generara un archivo db.json, ese es el archivo que debemos incluir en el repo del proyecto cuando enviemos la practica.

```bash
npm start
```
