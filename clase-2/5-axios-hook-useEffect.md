# Axios y hook useEffect:

En los componentes necesitamos un lugar donde sincronizar con efectos externos, como por ejemplo:

- Llamadas a APIs para carga de datos.
- Subscripciones a sockets, listeners.
- Inicialización de timeouts e intervalos.
- Integración de librerias de terceros.

El lugar para hacer eso es usar un hook llamado useEffect().

Lo que buscamos es traernos de una API los tweets para pintarlos. Quitaremos el arreglo que tenemos ahi hardcodeado y nos conectaremos a sparres.js.

Tambien para conectarnos a la api usaremos una libreria llamada axios que es muy parecida a fetch que es nativa pero que nos da muchas mas cosas.

Aqui tenemos la documentación de axios:

https://axios-http.com/es/docs/intro

Vamos a instalarla en el proyecto:

```bash
npm i axios
```

Dentro de src vamos a crearnos una carpeta api y dentro de esta carpeta un fichero llamado client.js, aqui meteremos la configuración de mi client para la api.

## api/client.js:

```javascript
// Importamos la libreria
import axios from 'axios';

// Creamos el cliente de axios:
const client = axios.create({
  baseURL: 'http://localhost:8000',
});

// Creamos un interceptor de la respuesta para sacar el data:
client.interceptors.response.use((response) => {
  return response.data;
});

export default client;
```

Vamos a ver como crear ahora variables de entorno, ya que vamos a cambiar del objeto de configuracion baseUrl por una variable de entorno asi no tendremos hardcodeado el host de la api y tenerlo que cambiar en varias partes de la app sino solo en el fichero de variables de entorno.

Nos creamos en la raiz del proyecto el archivo .env.development:

## .env.development:

```
VITE_API_BASE_URL=http://localhost:8000
```

Ahora para usar la variable de entorno usando vite seria asi:

## api/client.js:

```javascript
// Importamos la libreria
import axios from 'axios';

// Creamos el cliente de axios:
const client = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
});

// Creamos un interceptor de la respuesta para sacar el data:
client.interceptors.response.use((response) => {
  return response.data;
});

export default client;
```

NOTA: Si usamos CRA tendriamos que usar asi en .env.development:

```
REACT_APP_API_BASE_URL=http://localhost:8000
```

```javascript
import axios from 'axios';

// Creamos un cliente:
const client = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
});

// Del objeto que retorna axios sacada a data que es lo que nos interesa.
client.interceptors.response.use((response) => {
  return response.data;
});

export default client;
```

lo siguiente que haremos es crear en el componente que va a consumir ese cliente creado un fichero llamado service.js

## pages/tweets/service.js:

```javascript
// importamos el cliente:
import client from '../../api/client';

// definimos el end point de la api:
const tweetsUrl = '/api/tweets';

// definimos el metodo para hacer la peticion get a la api:
export const getLatestTweets = () => {
  return client.get(tweetsUrl);
};
```

Ahora desde nuestro componente hacemos la llamada: Debemos usar los hook de useState para poder cambiar el estado del componente y useState para la peticion a la api:

## pages/tweets/TweetsPage.jsx:

```javascript
// Importamos los estilos:
// import './TweetsPage.css';

// Importamos estilos usando modules css:
import styles from './TweetsPage.module.css';
// console.log(styles);

// importamos la libreria clsx:
import clsx from 'clsx';

// Importamos la llamada a la api:
import { getLatestTweets } from './service';
import { useEffect, useState } from 'react';

const TweetsPage = ({ dark }) => {
  // declaramos un state para los tweets:
  const [tweets, setTweets] = useState([]);

  const classClsx = clsx(styles.TweetsPage, {
    [styles.dark]: dark,
    [styles.light]: !dark,
  });

  // Llamamos al metodo para llamar a la api: usamos useEffect:
  useEffect(() => {
    getLatestTweets().then((tweets) => {
      setTweets(tweets);
    });
  }, []);

  return (
    <div className={classClsx}>
      <ul
        style={{ listStyle: 'none', padding: '30', border: '1px solid blue' }}
      >
        {tweets.map((tweet) => {
          return <li key={tweet.id}>{tweet.content}</li>;
        })}
      </ul>
    </div>
  );
};

export default TweetsPage;
```

Hemos quitado los tweet hardcodeados y hemos agregado esto en el componente:

## pages/tweets/TweetsPage.jsx:

```javascript
// Importamos la llamada a la api:
import { getLatestTweets } from './service';
import { useEffect, useState } from 'react';

// declaramos un state para los tweets:
const [tweets, setTweets] = useState([]);

// Llamamos al metodo para llamar a la api: usamos useEffect:
useEffect(() => {
  getLatestTweets().then((tweets) => {
    setTweets(tweets);
  });
}, []);
```

Como funciona useEffect:

Lo primero que hace react es renderizar el componente, en un primer render los tweets estan en un arreglo vacio porque asi se lo decimos en el estado, luego registra el efecto y una vez terminado el render ejecuta el efecto por ende carga de la api los tweets y los pinta, luego useEffect tiene un segundo parametro, para este caso es un arrrglo vacio de dependencias, esto quiere decir que el efecto solo se cargara en el primer render.

En React, el segundo parámetro de la función useEffect es la dependencia. La dependencia es un array opcional que especifica qué valores o variables deben ser monitoreados para que el efecto se vuelva a ejecutar.
Cuando la dependencia está presente, el efecto se ejecutará cada vez que la dependencia cambie entre renderizaciones. Si la dependencia está vacía ([]), el efecto se ejecutará solo una vez después del primer renderizado.

## Anatomia de useEffect:

- La funcion pasada a useEffect se ejcuta despues de cada render.
- El array de dependencias controla cuando se ejecuta:
  - undefined: no se pone el arreglo de dependencia, se ejecuta el efecto tras cada render,
  - []: se ejecuta solo despues del primer render.
  - [a,b]: se ejecuta solo si han cambiado alguno de los valores del array (a,b).
- Opcionalmente, el callback puede devolver una funcion de clena up o de limpieza, que se ejecutara antes de la proxima llamada al efecto. Entre otras cosas sirve para:
  - Cancelar suscripciones,
  - Liberar timeouts e intervalos,
  - Abortar peticiones al API pendientes, etc.

Vemos el ejemplo con dependencias:

## pages/tweets/TweetsPage.jsx:

```javascript
// Ejemplo de useEffect con dependencia:
useEffect(() => {
  document.title = dark ? 'dark' : 'light';

  // Funcion de limpieza: aqui no tiene mucho sentido pero es asi:
  return () => {
    console.log('Exit');
  };
}, [dark]);
```

Este efecto se ejecutara solo cuando la dependencia dark cambie, el código del efecto depende de dark. Y asiganara al titulo de la página dark o light.

Para más ejemplos los podemos ver en el pdf de explicación.

Hacemos un refactoring del codigo y lo podemos usar con async await:

## pages/tweets/TweetsPage.jsx:

```javascript
// Llamamos al metodo para llamar a la api: usamos useEffect: Async await:
useEffect(() => {
  const fetchTweets = async () => {
    const tweets = await getLatestTweets();
    setTweets(tweets);
  };

  fetchTweets();
}, []);
```
