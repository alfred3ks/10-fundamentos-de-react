import Layout from '../../../components/layout/Layout';

const NewTweetPage = () => {
  return <Layout title="What are you thinking">New Tweet Page</Layout>;
};

export default NewTweetPage;
