// Importamos los estilos:
// import './TweetsPage.css';

// Importamos estilos usando modules css:
import styles from './TweetsPage.module.css';
// console.log(styles);

// importamos la libreria clsx:
import clsx from 'clsx';

// Importamos la llamada a la api:
import { getLatestTweets } from './service';
import { useEffect, useState } from 'react';

// Importamos el boton:
import Button from '../../components/Button';

// Importamos el Layout:
import Layout from '../../components/layout/Layout';

// Componente TweetPage:
const TweetsPage = ({ dark }) => {
  // declaramos un state para los tweets:
  const [tweets, setTweets] = useState([]);

  const classClsx = clsx(styles.TweetsPage, {
    [styles.dark]: dark,
    [styles.light]: !dark,
  });

  // Llamamos al metodo para llamar a la api: usamos useEffect: Promesas:
  // useEffect(() => {
  //   getLatestTweets().then((tweets) => {
  //     setTweets(tweets);
  //   });
  // }, []);

  // Llamamos al metodo para llamar a la api: usamos useEffect: Async await:
  useEffect(() => {
    const fetchTweets = async () => {
      const tweets = await getLatestTweets();
      setTweets(tweets);
    };

    fetchTweets();
  }, []);

  // Ejemplo de useEffect con dependencia:
  useEffect(() => {
    document.title = dark ? 'dark' : 'light';

    // Funcion de limpieza: aqui no tiene mucho sentido pero es asi:
    return () => {
      console.log('Exit, función de limpieza.');
    };
  }, [dark]);

  // Transformamos el cero del arreglo a booleano:
  const hasTweets = !!tweets.length;
  // console.log(hasTweets);

  return (
    <Layout title="What's going on...">
      <div className={classClsx}>
        {/* Metemos un condicional para mostrar tweets o mensaje */}
        {hasTweets ? (
          <ul
            style={{
              listStyle: 'none',
              padding: '30',
              border: '1px solid blue',
            }}
          >
            {tweets?.map((tweet) => {
              return <li key={tweet.id}>{tweet.content}</li>;
            })}
          </ul>
        ) : (
          <Button $variant={'primary'}>Be the first one... </Button>
        )}
      </div>
    </Layout>
  );
};

export default TweetsPage;
