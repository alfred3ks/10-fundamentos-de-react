import client, {
  setAuthorizationHeader,
  removeAuthorizationHeader,
} from '../../api/client';
import storage from '../../utils/storage';

export const login = (credentials) => {
  // Usamos el cliente se axios con el metodo post a la url pasandole las credenciales:
  return client.post('/auth/login', credentials).then(({ accessToken }) => {
    // Guardamos en axios el token:
    setAuthorizationHeader(accessToken);

    // Persistimos el token en el storage:
    storage.set('auth', accessToken);
  });
};

// Funcion Async await para el login:
// export const login = async (credentials) => {
//   try {
//     // Utilizamos el cliente de Axios con el método post en la URL pasándole las credenciales
//     const response = await client.post('/auth/login', credentials);
//     console.log(response);
//     // Puedes devolver la respuesta si es necesario
//     return response;
//   } catch (error) {
//     // Manejar errores aquí
//     console.error('Error al iniciar sesión:', error);
//     // Puedes lanzar el error nuevamente si necesitas que sea manejado en otra parte del código
//     throw error;
//   }
// };

// metodo para hacer logout:
export const logout = () => {
  return Promise.resolve().then(() => {
    // quitamos el token de axios:
    removeAuthorizationHeader();

    // quitamos el token de localstorage
    storage.remove('auth');
  });
};

// Funcion asincrona de logout:
// export const logout = async () => {

//   // Quitamos el token de axios:
//   removeAuthorizationHeader();

//   // Quitamos el token de localstorage
//   storage.remove('auth');
// };
