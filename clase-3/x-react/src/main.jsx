import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import storage from './utils/storage.js';
import { setAuthorizationHeader } from './api/client.js';
import AuthContextProvider from './pages/auth/AuthContextProvider.jsx';

// Revisamos si hay token:
const accedToken = storage.get('auth');

if (accedToken) {
  // guardamos en axios:
  setAuthorizationHeader(accedToken);
}

// Convertimos el token en booleano: Sera true o false:
const isUserLogged = !!accedToken;

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AuthContextProvider initiallyLogged={isUserLogged}>
      <App />
    </AuthContextProvider>
  </React.StrictMode>
);
