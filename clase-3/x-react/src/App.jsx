import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';
import { useAuth } from './pages/auth/context';

const App = () => {
  const { isLogged } = useAuth();
  return (
    <div>
      {isLogged ? (
        <>
          <TweetsPage dark={false} />
        </>
      ) : (
        <LoginPage />
      )}
    </div>
  );
};

export default App;
