// Importamos la libreria
import axios from 'axios';

// Creamos el cliente de axios:
const client = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
});

// Creamos un interceptor de la respuesta para sacar el data:
client.interceptors.response.use((response) => {
  return response.data;
});

// Método para guardar el token en axios:
export const setAuthorizationHeader = (token) =>
  (client.defaults.headers.common['Authorization'] = `Bearer ${token}`);

// Método para quitar el token de axios:
export const removeAuthorizationHeader = () => {
  delete client.defaults.headers.common['Authorization'];
};

export default client;
