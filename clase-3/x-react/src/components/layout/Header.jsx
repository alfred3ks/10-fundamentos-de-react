import Button from '../Button';
import Icon from '../../components/Icon';
import { logout } from '../../pages/auth/service';
import { useAuth } from '../../pages/auth/context';

const Header = () => {
  // Recibimos del contexto isLogged y onLogout usando el custom hook:
  const { isLogged, onLogout } = useAuth();

  // Creamos la funcion para el onClick del boton de logout:
  const handleLogout = async () => {
    // borramos token de localStorage y axios:
    await logout();
    // Disparamos el evento para cambiar el estado:
    onLogout();
  };

  return (
    <header>
      <div>
        <Icon width={32} fill="crimson" />
      </div>

      <nav>
        {isLogged ? (
          <Button onClick={handleLogout}>Logout</Button>
        ) : (
          <Button $variant="primary">Login</Button>
        )}
      </nav>
    </header>
  );
};

export default Header;
