const Footer = ({ children }) => {
  const currentYear = new Date().getFullYear();

  return (
    <footer>
      @{currentYear} {children}
    </footer>
  );
};

export default Footer;
