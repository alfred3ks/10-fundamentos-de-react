import { useState, useEffect } from 'react';

const Reloj = () => {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    // Actualizar la hora cada segundo
    const intervalId = setInterval(() => {
      setTime(new Date());
    }, 1000);

    // Limpiar el intervalo cuando el componente se desmonta
    return () => clearInterval(intervalId);
  }, []); // El array vacío asegura que el efecto solo se ejecute una vez al montar el componente

  // Obtener componentes de hora, minutos y segundos
  const hours = time.getHours();
  const minutes = time.getMinutes();
  const seconds = time.getSeconds();

  // formateamos los segundos siempre a dos digitos:
  const segundsFormat = seconds < 10 ? `0${seconds}` : seconds;

  // formateamos los minutos siempre a dos digitos:
  const minutesFormat = minutes < 10 ? `0${minutes}` : minutes;

  return (
    <div>
      <h2>Clock</h2>
      <p>{`${hours}:${minutesFormat}:${segundsFormat}`}</p>
    </div>
  );
};

export default Reloj;
