# Boton de Logout:

Ahora vamos a colocar un boton de logout para borrar el token y redirigir al componente de login.

De momento ese boton lo vamos a meter debajo del listado de tweet en el componente TweetsPage.jsx:

## TweetsPage.jsx:

```javascript
// Importamos el boton:
import Button from '../../components/Button';

<Button>Logout</Button>;
```

Cuando demos click al boton de logout lo que haremos es borrar el token de localStorage, borramos el token de axios, cambiamos el estado para mostrar solo el formulario de login.

Debemos crear un método para borrar de axios el token, lo hacemos donde obtenemos el token y se lo pasamos a axios en api/cliente:

## api/client.js:

```javascript
// Método para quitar el token de axios:
export const removeAuthorizationHeader = () => {
  delete client.defaults.headers.common['Authorization'];
};
```

En service.js de LoginPage.jsx tenemos el metodo para hacer login, pues vamos a hacer el método para hacer logout:

## page/auth/service.js:

```javascript
// metodo para hacer logout:
export const logout = () => {
  return Promise.resolve().then(() => {
    // quitamos el token de axios:
    removeAuthorizationHeader();

    // quitamos el token de localstorage
    storage.remove('auth');
  });
};
```

De acuerdo ya tenemos los metodos ahora hay que hacer que cuando se pulse el boton se lancen esos métodos. Nos vamos a App.jsx: Creamos el evento para hacer logout y se lo pasamos por props al componente TweetsPage.jsx que es donde esta el boton.

## App.jsx:

```javascript
// declaramos nuestro evento para hacer logout:
const handleLogout = () => {
  setIsLogged(false);
};

<TweetsPage dark={false} onLogout={handleLogout} />;
```

Y lo recibimos por props al componente TweetsPage.jsx:

## TweetsPage.jsx:

```javascript
import { logout } from '../auth/service';

const TweetsPage = ({ dark, onLogout }) => {
  // Creamos la funcion para el onClick del boton de logout:
  const handleLogout = async () => {
    // borramos token de localStorage y axios:
    await logout();
    // Disparamos el evento para cambiar el estado:
    onLogout();
  };

  <Button onClick={handleLogout}>Logout</Button>;
};
```

Lo probamos y vemos que funciona...
