# Strict mode: vigilante del código de desarrollo:

El componente <StrictMode></StrictMode> permite encontrar bugs durante el desarrollo, en producción no se crea en el bundle, este componente nos permite:

- Chequeo del uso de métodos obsoletos en componentes.
- Re-render de componentes para detectar bugs,
- Re-ejecución para efectos para detectar bugs.

Se recomienda que siempre en desarrollo usar este componente. Lo vemos que tenemos en main.jsx que envuelve a toda nuestra aplicación.

## main.jsx:

```javascript
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App initiallyLogged={isUserLogged} />
  </React.StrictMode>
);
```
