# Condicional para mostrar si hay tweets:

Cuando hacemos una peticion a la Api para que nos muestre los tweets puede darse el caso que no existan tweets y si no hay se mostraria en nuestro componente de TweetsPage.jsx una caja vacia, lo que haremos es mostrar un mensaje si no hay tweets y si los hay mostrara estos. Tamnbien he aprovechado y he colocado el optional chaining cuando hacemos el bucle map del arreglo de los tweets.

Cuando hagamos un componente que carga datos asincronamente debemos intentar pensar en todos sus estados, el estado normal seria mostrar los tweets que hay en la bd, puede darse el caso que nuestra app tenga la necesidad de mostrar un estado vacio y lo peor que podemos hacer es dejar una pantalla en blanco al usuario. Debemos controlar esos estados.

Si no hay tweets mostraremos algo. Un mensaje, puede ser un componente. O un boton, para este caso que le va a rederigir a la pagina de crear un tweet.

## TweetPage.jsx:

```javascript
// Transformamos el cero del arreglo a booleano:
const hasTweets = !!tweets.length;
console.log(hasTweets);

return (
  <Layout title="What's going on...">
    <div className={classClsx}>
      {/* Metemos un condicional para mostrar tweets o mensaje */}
      {hasTweets ? (
        <ul
          style={{
            listStyle: 'none',
            padding: '30',
            border: '1px solid blue',
          }}
        >
          {tweets?.map((tweet) => {
            return <li key={tweet.id}>{tweet.content}</li>;
          })}
        </ul>
      ) : (
        <Button $variant={'primary'}>Be the first one... </Button>
      )}

      <Button onClick={handleLogout}>Logout</Button>
    </div>
  </Layout>
);
```

El componente entero queda asi:

## TweetPage.jsx:

```javascript
// Importamos los estilos:
// import './TweetsPage.css';

// Importamos estilos usando modules css:
import styles from './TweetsPage.module.css';
// console.log(styles);

// importamos la libreria clsx:
import clsx from 'clsx';

// Importamos la llamada a la api:
import { getLatestTweets } from './service';
import { useEffect, useState } from 'react';

// Importamos el boton:
import Button from '../../components/Button';
import { logout } from '../auth/service';

// Importamos el Layout:
import Layout from '../../components/layout/Layout';

// Componente TweetPage:
const TweetsPage = ({ dark, onLogout }) => {
  // declaramos un state para los tweets:
  const [tweets, setTweets] = useState([]);

  const classClsx = clsx(styles.TweetsPage, {
    [styles.dark]: dark,
    [styles.light]: !dark,
  });

  // Llamamos al metodo para llamar a la api: usamos useEffect: Promesas:
  // useEffect(() => {
  //   getLatestTweets().then((tweets) => {
  //     setTweets(tweets);
  //   });
  // }, []);

  // Llamamos al metodo para llamar a la api: usamos useEffect: Async await:
  useEffect(() => {
    const fetchTweets = async () => {
      const tweets = await getLatestTweets();
      setTweets(tweets);
    };

    fetchTweets();
  }, []);

  // Ejemplo de useEffect con dependencia:
  useEffect(() => {
    document.title = dark ? 'dark' : 'light';

    // Funcion de limpieza: aqui no tiene mucho sentido pero es asi:
    return () => {
      console.log('Exit, función de limpieza.');
    };
  }, [dark]);

  // Creamos la funcion para el onClick del boton de logout:
  const handleLogout = async () => {
    // borramos token de localStorage y axios:
    await logout();
    // Disparamos el evento para cambiar el estado:
    onLogout();
  };

  // Transformamos el cero del arreglo a booleano:
  const hasTweets = !!tweets.length;
  console.log(hasTweets);

  return (
    <Layout title="What's going on...">
      <div className={classClsx}>
        {/* Metemos un condicional para mostrar tweets o mensaje */}
        {hasTweets ? (
          <ul
            style={{
              listStyle: 'none',
              padding: '30',
              border: '1px solid blue',
            }}
          >
            {tweets?.map((tweet) => {
              return <li key={tweet.id}>{tweet.content}</li>;
            })}
          </ul>
        ) : (
          <Button $variant={'primary'}>Be the first one... </Button>
        )}
        <Button onClick={handleLogout}>Logout</Button>
      </div>
    </Layout>
  );
};

export default TweetsPage;
```

OJO esto en la práctica lo dice que pensemos que mostrar al usuario si no hay información en la BD.
