# Refactoring context - custom hook:

Tal como tenemos establecido actualmente el contexto en nuestra aplicación funciona y va bien, pero lo que se suele hacer es mejorarlo. Como vemos en Header.jsx y en LoginPage.jsx para sacar la información del contexto debemos importar el hook de useContex() y importar el context AuthContext. Cuando tengamos una situación podriamos crearnos un custom hook.

Asi ya vemos como crear un custom hook. lo creamos dentro del archivo context.js

## context.js:

```javascript
import { createContext, useContext } from 'react';

// creamos el contexto:
export const AuthContext = createContext(false);

// Creamos un custom hook:
export const useAuth = () => {
  const auth = useContext(AuthContext);
  return auth;
};
```

Ahora modificamos LoginPage.jsx:

## LoginPage.jsx:

```javascript
import { useContext, useState } from 'react';
import Button from '../../components/Button';
import { login } from './service';
import { useAuth } from './context';

const LoginPage = () => {
  // Recibimos del contexto onLogin con el custom hook:
  const { onLogin } = useAuth();
```

Igual hacemos con Header.jsx:

## Header.jsx

```javascript
import Button from '../Button';
import Icon from '../../components/Icon';
import { logout } from '../../pages/auth/service';
import { useAuth } from '../../pages/auth/context';

const Header = () => {
  // Recibimos del contexto isLogged y onLogout usando el custom hook:
  const { isLogged, onLogout } = useAuth();
```

Ahora ya tenemos mucho mas desacoplada nuestra aplicacion.
