# Reglas de los hooks:

React se basa en el orden en que los hooks estan escritos para mantener internamente los valores (estado, efectos...).

Por ello, debemos seguir ciertas reglas cuando usamos hooks:

- Solo pueden ser usados dentro de componentes o de otros hooks. No pueden ser usados en funciones 'normales' o clases.

- Dentro del componente (o de otro hook), no pueden ser usados dentro de bucles, condicionales o funciones internas.

- Para asegurarnos que usamos hooks correctamente existe el plugin eslint que nos avisa de la violación de estas reglas, asi como el correcto uso de las dependencias en hooks como useEffect.

- Podemos crear nuestros propios custom hooks
