# El contexto, react context:

Pasar props de padres a hijos se complica cuando el arbol crece y es muy propenso a errores, esto es lo que se llama props drilling.

Componentes intermedios tienen que pasar información por props. Cuando esa información a ellos no les sirve para nada.

Para solucionar el props drilling tenemos React context, el cual permite a un componente compartir datos directamente con todos los componentes que esten debajo de el.

![](./img/context.png)

Con React context hacemos un tunel de donde esta la información hasta el componente que necesita esa información, tal como vemos en la imagen.

## Pasos para usar React context:

- Paso 1: Crear el contexto:

Creamos el contexto con createContext pasando por default value, este valor por defecto es un valor estatico que usara React cuando encuentre el provider del contexto.

## ThemeContext.jsx:

```javascript
import { createContex } from 'react';

export const ThemeContext = createContex('light');
```

- Paso 2: Consumir el contexto: El componente que consume el contexto:

Consumimos el contexto en cualquier componente mediante el hook useContext(). Un cambio en el contexto forzara un render del componente.

## MyButton.jsx:

```javascript
import { useContext } from 'react';
import { ThemeContext } from './ThemeContext';

const myButton = () => {
  const theme = useContext(ThemeContext);
};
```

- Paso 3: Proveer el contexto:

Proveer el valor del contexto con Context.Provider. Todos los componentes que queden dentro del provider tendran acceso al valor del contexto:

## ThemeProvider.jsx:

```javascript
import { ThemeContext } from './ThemeContext';

export default function ThemeProvider({ children, theme }) {
  return (
    <ThemeContext.provider value={theme}>{children}</ThemeContext.provider>
  );
}
```

## App.jsx o cualquier componente donde queramos el provider del contexto:

```javascript
import ThemeProvider from './ThemeProvider';

expor default function App(){
  return (
    <ThemeProvider value='light'><MyButton/></ThemeProvider>
  )
}
```

Existen 3 formas por las cuales un componente se puede renderizar:

- Porque cambie su estado,
- Porque se renderice el padre,
- Porque este conectado a un contexto y el valor del contexto cambie.

Vamos a pasar a contexto la autenticacion. Dentro de la carpeta auth creamos un fichero llamado context.js

## context.js:

```javascript
import { createContext } from 'react';

// creamos el contexto:
export const AuthContext = createContext(false);
```

Tenemos que pensar donde colocar el provider.
Ahora nos vamos al componente App.jsx: y envolvemos aquellos componentes que tendran acceso al contexto:

## App.jsx:

```javascript
import { useState } from 'react';
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';
import { AuthContext } from './pages/auth/context';

const App = ({ initiallyLogged }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);

  // declaramos nuestro evento para hacer logout:
  const handleLogout = () => {
    setIsLogged(false);
  };

  // Creamos un objeto que pasaremos al value:
  const authValue = {
    isLogged,
    onLogin: handleLogin,
    onLogout: handleLogout,
  };

  return (
    <AuthContext.Provider value={authValue}>
      <div>
        {isLogged ? (
          <>
            <TweetsPage
              dark={false}
              onLogout={handleLogout}
              isLogged={isLogged}
            />
          </>
        ) : (
          <LoginPage onLogin={handleLogin} />
        )}
      </div>
    </AuthContext.Provider>
  );
};

export default App;
```

Como podemos ver hemos creado un objeto que llamamos:

```javascript
const authValue = {
  isLogged,
  onLogin: handleLogin,
  onLogout: handleLogout,
};
```

En este objeto metemos todas aquellas variables, funciones que necesitamos consumir en nuestros componentes y los componentes los envolvemos por medio del componente:

```javascript
<AuthContext.Provider value={authValue}></AuthContext.Provider>
```

Ahora nos vamos al componente Header que es donde vamos a recibir isLogged y onlogout y lo recibimos de la siguiente manera:

```javascript
import { useContext } from 'react';

// Recibimos del contexto isLogged y onLogout:
const { isLogged, onLogout } = useContext(AuthContext);
```

Y nuestro componente Header ya no tiene que recibir mas por props, nos quedaria asi:

## Header.jsx:

```javascript
import Button from '../Button';
import Icon from '../../components/Icon';
import { logout } from '../../pages/auth/service';
import { AuthContext } from '../../pages/auth/context';
import { useContext } from 'react';

const Header = () => {
  // Recibimos del contexto isLogged y onLogout:
  const { isLogged, onLogout } = useContext(AuthContext);

  // Creamos la funcion para el onClick del boton de logout:
  const handleLogout = async () => {
    // borramos token de localStorage y axios:
    await logout();
    // Disparamos el evento para cambiar el estado:
    onLogout();
  };

  return (
    <header>
      <div>
        <Icon width={32} fill="crimson" />
      </div>

      <nav>
        {isLogged ? (
          <Button onClick={handleLogout}>Logout</Button>
        ) : (
          <Button $variant="primary">Login</Button>
        )}
      </nav>
    </header>
  );
};

export default Header;
```

Ahora donde se pasan las props las quitamos desde App.jsx hasta Header.jsx. Vemos que ahora ya quitamos este props drillling. Y sigue funcionando la aplicación.

Tambien los hacemos con el componente LoginPage.jsx consumimos el contexto:

## LoginPage.jsx:

```javascript
import { useContext, useState } from 'react';
import Button from '../../components/Button';
import { login } from './service';
import { AuthContext } from './context';

const LoginPage = () => {
  // Recibimos del contexto onLogin:
  const { onLogin } = useContext(AuthContext);

  // Estado unificado para los imputs:
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });

  // Funcion handle del formulario
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
    await login(credentials);

    // aqui el usuario ya esta logueado: Lanzamos el evento que viene del padre:
    onLogin();
  };

  // Creamos la funcion para los inputs controlados:
  const handleChange = (e) => {
    setCredentials((currentCredentials) => ({
      ...currentCredentials,
      [e.target.name]: e.target.value,
    }));
  };

  // Hacemos destructuring de las credenciales:
  const { username, password } = credentials;
  // Creamos la variable para el boton: esta linea de ejecuta en cada render
  const disabled = !(username && password);

  return (
    <div>
      <h1>Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="username"
          id="username"
          onChange={handleChange}
          value={username}
        />
        <input
          type="password"
          name="password"
          id="password"
          onChange={handleChange}
          value={password}
        />
        <Button type="submit" $variant="primary" disabled={disabled}>
          Log in
        </Button>
      </form>
    </div>
  );
};

export default LoginPage;
```

Hemos quitado la props y consumido del constexto la funcion onLogin:

```javascript
import { useContext, useState } from 'react';
// Recibimos del contexto onLogin:
const { onLogin } = useContext(AuthContext);
```

Ya podemos en App.jsx quitar la props que pasamos al componente, vemos como nos queda el componente, todo limpio y con el contexto:

## App.jsx:

```javascript
import { useState } from 'react';
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';
import { AuthContext } from './pages/auth/context';

const App = ({ initiallyLogged }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);

  // declaramos nuestro evento para hacer logout:
  const handleLogout = () => {
    setIsLogged(false);
  };

  // Creamos un objeto que pasaremos al value:
  const authValue = {
    isLogged,
    onLogin: handleLogin,
    onLogout: handleLogout,
  };

  return (
    <AuthContext.Provider value={authValue}>
      <div>
        {isLogged ? (
          <>
            <TweetsPage dark={false} />
          </>
        ) : (
          <LoginPage />
        )}
      </div>
    </AuthContext.Provider>
  );
};

export default App;
```

Ya hemos desacoplado nuestros componentes, la informacion la consumen del contexto y ya no dependen de recibir la información por props.
