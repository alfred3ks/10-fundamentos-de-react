## Componente provider:

Vamos a hacer un refactoring donde metemos el provider, en App.jsx. Vamos a crearnos un solo componente que tenga la responsabilidad de manejar el estado y el contexto.

Para esto nos vamos a crear un componente dentro de la carpeta auth que vamos a llamar AuthContextProvider.jsx

## AuthContextProvider.jsx:

```javascript
// componente provider:
import { useState } from 'react';
import { AuthContext } from './context';

const AuthContextProvider = ({ initiallyLogged, children }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);
  // Método para hacer login:
  const handleLogin = () => {
    setIsLogged(true);
  };

  // Método para hacer logout:
  const handleLogout = () => {
    setIsLogged(false);
  };

  // Creamos un objeto que pasaremos al value:
  const authValue = {
    isLogged,
    onLogout: handleLogout,
    onLogin: handleLogin,
  };
  return (
    <AuthContext.Provider value={authValue}>{children}</AuthContext.Provider>
  );
};

export default AuthContextProvider;
```

Ahora en App.jsx hacemos limpia y tenemos que rodear a nuestro componente App.jsx con este componente en main.jsx, importamos el componente y rodeamos a App.jsx:

## main.jsx

```javascript
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import storage from './utils/storage.js';
import { setAuthorizationHeader } from './api/client.js';
import AuthContextProvider from './pages/auth/AuthContextProvider.jsx';

// Revisamos si hay token:
const accedToken = storage.get('auth');

if (accedToken) {
  // guardamos en axios:
  setAuthorizationHeader(accedToken);
}

// Convertimos el token en booleano: Sera true o false:
const isUserLogged = !!accedToken;

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AuthContextProvider initiallyLogged={isUserLogged}>
      <App />
    </AuthContextProvider>
  </React.StrictMode>
);
```

Luego App.jsx nos quedaria mucho mas limpio:

## App.jsx:

```javascript
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';
import { useAuth } from './pages/auth/context';

const App = () => {
  const { isLogged } = useAuth();
  return (
    <div>
      {isLogged ? (
        <>
          <TweetsPage dark={false} />
        </>
      ) : (
        <LoginPage />
      )}
    </div>
  );
};

export default App;
```

Ya tenemos toda nuestra aplicacion envuelta por un componente provider.
