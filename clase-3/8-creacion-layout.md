# Creación del Layout:

Vamos a crear los layout, es codigo que se va a repetir en las diferentes paginas como podria ser el header y el footer.

Dentro de la carpeta component vamos a crearnos una que llamaremos layout. Nos crearemos los componentes:

- Layout.jsx
- Header.jsx
- Footer.jsx

## Footer.jsx:

```javascript
const Footer = ({ children }) => {
  const currentYear = new Date().getFullYear();

  return (
    <Footer>
      @{currentYear} {children}
    </Footer>
  );
};

export default Footer;
```

## Header.jsx:

```javascript
import Button from '../Button';
import logo, {
  ReactComponent as Icon,
} from '../../../public/images/logo-x.svg';
// console.log(logo); // http://localhost:5173/public/images/logo-x.svg

const Header = () => {
  return (
    <header>
      <div>
        <Icon width={32} height={32} />
        <img src={logo} alt="logo de x" width={45} />
      </div>

      <nav>
        <Button $variant="primary">Log in</Button>
      </nav>
    </header>
  );
};

export default Header;
```

Vale esta opcion que vemos funciona bien para importar el .svg como <Icon/> en CRA pero en vite no funciona da error. En Vite es diferente, para esto la mejor opcion es crear un componente que devuelva el .svg. Lo llamaremos Icon.jsx y lo meteremos en la carpeta component:

## Icon.jsx:

```javascript
const Icon = (props) => {
  return (
    <svg
      id="Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 118.98 107.54"
      {...props}
    >
      <path
        d="M290.21,202.23h18.24L268.6,247.78l46.89,62H278.77L250,272.18l-32.9,37.6H198.86L241.49,261l-45-58.82h37.65l26,34.36Zm-6.4,96.62h10.11l-65.25-86.28H217.82Z"
        transform="translate(-196.51 -202.23)"
      />
    </svg>
  );
};

export default Icon;
```

Este icono como podemos ver le pasamos las props, luego lo importo y lo usamos en el Header.jsx:

## Header.jsx:

```javascript
import Button from '../Button';
import Icon from '../../components/Icon';

const Header = () => {
  return (
    <header>
      <div>
        <Icon width={32} fill="crimson" />
      </div>

      <nav>
        <Button $variant="primary">Log in</Button>
      </nav>
    </header>
  );
};

export default Header;
```

Ya tenemos el Header.jsx y el Footer.jsx ahora tenemos que crear el Layout.jsx.

## Layout.jsx:

```javascript
import Footer from './Footer';
import Header from './Header';

const Layout = ({ title, children }) => {
  return (
    <div>
      <Header />
      <main>
        <h2>{title}</h2>
        {children}
      </main>
      <Footer>KeepCoding</Footer>
    </div>
  );
};

export default Layout;
```
