# Creación del componente NewTweetPage.jsx:

Vamos a crearnos un nuevo componente, sera el de creación de tweets, lo llamaremos NewTweetPage.jsx y lo meteremos en la carpeta de tweets: Yo he optado por crearme una carpeta llamandolo como en componente y internamente le he puesto un index.jsx, asi seria mas practico para organizar todo el componente con su css dentro de la carpeta:

## NewTweetPage/index.jsx:

```javascript
import Layout from '../../../components/layout/Layout';

const NewTweetPage = () => {
  return <Layout title="What are you thinking">New Tweet Page</Layout>;
};

export default NewTweetPage;
```

Ahora tenemos otro componente que tenemos que comparte el mismo layout. ahora lo que haremos en app.jsx mostrar este componente. Sinceramente es un paso que no lo veo pero lo hace para poder mostrar dos layouts. En fin..

## App.jsx:

```javascript
const App = ({ initiallyLogged }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);

  // declaramos nuestro evento para hacer logout:
  const handleLogout = () => {
    setIsLogged(false);
  };

  return (
    <div>
      {isLogged ? (
        <>
          <TweetsPage dark={false} onLogout={handleLogout} />
          <NewTweetPage />
        </>
      ) : (
        <LoginPage onLogin={handleLogin} />
      )}
    </div>
  );
};
```
