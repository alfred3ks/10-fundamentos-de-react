# Refactoring del formulario de inputs controlados:

Lo que vamos a hacer es crear un solo estado para los inputs en forma d objeto, ahgora solo tenemos 2 inputs pero los formularios suelen tener muchos inputs y vas aparatados y seria crear muchos estados, lo vamos a poner a un solo estado y una sola funcion handle:

## LoginPage.jsx:

```javascript
// Estado unificado para los imputs:
const [credentials, setCredentials] = useState({
  username: '',
  password: '',
});

// Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
await login(credentials);

// Creamos la funcion para los inputs controlados:
const handleChange = (e) => {
  setCredentials({
    ...credentials,
    [e.target.name]: e.target.value,
  });
};

  const { username, password } = credentials;
  // Creamos la variable para el boton: esta linea de ejecuta en cada render
  const disabled = !(username && password);

<input
  type="text"
  name="username"
  id="username"
  onChange={handleChange}
  value={username}
/>
<input
  type="password"
  name="password"
  id="password"
  onChange={handleChange}
  value={password}
/>
```

Nuestro componente LoginPage.jsx nos quedaria asi:

## LoginPage.jsx:

```javascript
import { useState } from 'react';
import Button from '../../components/Button';
import { login } from './service';

const LoginPage = ({ onLogin }) => {
  // Estado unificado para los imputs:
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });

  // Funcion handle del formulario
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
    await login(credentials);

    // aqui el usuario ya esta logueado: Lanzamos el evento que viene del padre:
    onLogin();
  };

  // Creamos la funcion para los inputs controlados:
  const handleChange = (e) => {
    setCredentials((currentCredentials) => ({
      ...currentCredentials,
      [e.target.name]: e.target.value,
    }));
  };

  // Hacemos destructuring de las credenciales:
  const { username, password } = credentials;
  // Creamos la variable para el boton: esta linea de ejecuta en cada render
  const disabled = !(username && password);

  return (
    <div>
      <h1>Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="username"
          id="username"
          onChange={handleChange}
          value={username}
        />
        <input
          type="password"
          name="password"
          id="password"
          onChange={handleChange}
          value={password}
        />
        <Button type="submit" $variant="primary" disabled={disabled}>
          Log in
        </Button>
      </form>
    </div>
  );
};

export default LoginPage;
```

Aqui vemos la explicación de la funcion handleChange:

```javascript
// Creamos la funcion para los inputs controlados:
const handleChange = (e) => {
  setCredentials((currentCredentials) => ({
    ...currentCredentials,
    [e.target.name]: e.target.value,
  }));
};
```

A la funcion setCredencials le pasamos una funcion que recibe las credenciales actuales y retorna de nuevo el objeto con el valor que solo cambia para cada input. Hacemos un spread del objeto y luego le pisamos una propiedad dinamicamente.
