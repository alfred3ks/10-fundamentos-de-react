# Implementación del Layout:

Ya tenemos el layout, ahora vamos a usarlo. De momento solo lo podemos usar en el componente TweetsPage.jsx, eso es porque el componente Login.jsx no llevara Layout.

## TweetPage.jsx:

```javascript
// Importamos el Layout:
import Layout from '../../components/layout/Layout';

const TweetsPage = ({ dark, onLogout }) => {
  return (
    <Layout title="What's going on...">
      <div className={classClsx}>
        <ul
          style={{ listStyle: 'none', padding: '30', border: '1px solid blue' }}
        >
          {tweets.map((tweet) => {
            return <li key={tweet.id}>{tweet.content}</li>;
          })}
        </ul>
        <Button onClick={handleLogout}>Logout</Button>
      </div>
    </Layout>
  );
};
```

Solo pongo el código que he agregado para ver todo el componente lo podemos ver en la aplicacion.
