# Header boton de login:

Actualmente nuestro boton de login del header no hace nada lo que haremos es que este boton al hacer click mande al formulario de logi y se cambie a logout si el usuario no esta logueado.

Vamos a quitar el boton actual de logout que tenemos porque la loógica la pondremos en ese boton del header. El boton de logout actual lo tenemos en el componente TweetPage.jsx, lo quitamos. Tambien tenemos la funcion handle que hace el logout.

Meteemos la logica del boton de logout en header pero podriamos extraerlo a un componente externo. Como veamos.

Para saber que el boton se pinde de una forma u otra necesitamos saber si hay un token en localStorage, asi sabremos que el usuario esta logueado.

Si esta logueado pintaremos el boton de logout y sino lo esta el de login. Solo hay una manera que react pasa informacion. Es de un componente padre a sus hijos, Vamos a tener que pasar por props desde App.jsx hasta Header.jsx la variable isLogged donde en el estado sabemos si el usuario esta logueado o no. Asi seria el paso de la props:

App.jsx tenemos el valor de que el usuario esta logueado y le pasamos por props al componente TweetPage.jsx:

## App.jsx:

```javascript
const App = ({ initiallyLogged }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);

  // declaramos nuestro evento para hacer logout:
  const handleLogout = () => {
    setIsLogged(false);
  };

  return (
    <div>
      {isLogged ? (
        <>
          <TweetsPage
            dark={false}
            onLogout={handleLogout}
            isLogged={isLogged}
          />
        </>
      ) : (
        <LoginPage onLogin={handleLogin} />
      )}
    </div>
  );
};
```

En el componente TweetPage.jsx lo recibe y lo pasa a Layout:

## TweetPage.jsx:

```javascript
// Componente TweetPage:
const TweetsPage = ({ dark, onLogout, isLogged }) => {
  return (
    <Layout title="What's going on..." isLogged={isLogged}>
      <div className={classClsx}>
        {/* Metemos un condicional para mostrar tweets o mensaje */}
        {hasTweets ? (
          <ul
            style={{
              listStyle: 'none',
              padding: '30',
              border: '1px solid blue',
            }}
          >
            {tweets?.map((tweet) => {
              return <li key={tweet.id}>{tweet.content}</li>;
            })}
          </ul>
        ) : (
          <Button $variant={'primary'}>Be the first one... </Button>
        )}
        {/* <Button onClick={handleLogout}>Logout</Button> */}
      </div>
    </Layout>
  );
};
```

Ahora el componente Layout pasa esa props al Header:

## Layout.jsx:

```javascript
const Layout = ({ title, children, isLogged }) => {
  return (
    <div>
      <Header isLogged={isLogged} />
      <main>
        <h2>{title}</h2>
        {children}
      </main>
      <Footer>KeepCoding</Footer>
    </div>
  );
};
```

Y el Header.jsx consume el props:

## Header.jsx:

```javascript
const Header = ({ isLogged }) => {
  console.log(isLogged);
  return (
    <header>
      <div>
        <Icon width={32} fill="crimson" />
      </div>

      <nav>
        {isLogged ? (
          <Button>Logout</Button>
        ) : (
          <Button $variant="primary">Login</Button>
        )}
      </nav>
    </header>
  );
};
```

Aqui lo que vemos es el tipico caso de Props drilling, vemos que desde App.jsx hemos pasado informacion que ha pasado por otros componentes que solo han servido para pasar informacion, solo eso, ahora solo son pocos componentes pero cuando la app se hace mas compleja esto es un problema.

Toca poner la logica del boton para hacer logout.

## Header.jsx:

```javascript
const Header = ({ isLogged, onLogout }) => {
  // Creamos la funcion para el onClick del boton de logout:
  const handleLogout = async () => {
    // borramos token de localStorage y axios:
    await logout();
    // Disparamos el evento para cambiar el estado:
    onLogout();
  };

  return (
    <header>
      <div>
        <Icon width={32} fill="crimson" />
      </div>

      <nav>
        {isLogged ? (
          <Button onClick={handleLogout}>Logout</Button>
        ) : (
          <Button $variant="primary">Login</Button>
        )}
      </nav>
    </header>
  );
};
```

Como vemos agregamos el evento onclick al boton de logout y su respectivo handleLogout, aqui vemos que hemos tenido que pasar por props tambien la funcion onLogout() que cambia el estado y que viene desde App.jsx.

Probamos el boton y funciona.
