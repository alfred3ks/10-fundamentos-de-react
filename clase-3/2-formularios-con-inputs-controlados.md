# Formularios con inputs controlados:

Ya hemos visto como manejar un formaulario no controlado en las clases anteriores, el formulario del login nosotros no tenemos ahora control sobre la información que nos pasa el usuario. Con los formularios controlados si podemos hacerlo.

Tambien vemos que no estamos manejando el error si los datos metidos no son validos.

Cosas que vemos en nuestro formulario actual el usuario podria hacer login sin datos metidos en los inputs, lo que hremos es que el boton se deshabilite siempre y solo se habilitara si se meten datos en los inputs.

## Inputs controlados:

Para poder usar inputs controlados debemos usar el evento onChange={}, le pasamos un funcion handle y por medio del evento (e) podemos saber el e.target.value, obtenemos el valor del input.

Ahora ya sabemos el valor de los inputs, con eso ya podemos ocultar el boton. si tenemos valor en ambos input habilitamos sino deshabilitado.

Lo que hacemos es al boton pasarle el atributo disabled, con eso ya lo tenemos deshabilitado. Para eso necesitamos establecer estado, para pasar a disabled a true.

Creamos el estado para los imputs, luego en las funciones handles de cada inputs establecemos los set y para el boton creamos una variable que pasaria a ser true si ambos valores del username y passwors son true:

## LoginPage.jsx:

```javascript
// Estado para los imputs:
const [username, setUsername] = useState('');
const [password, setPassword] = useState('');

// Creamos la funcion para los inputs controlados:
const handleUsernameChange = (e) => {
  setUsername(e.target.value);
  console.log(e.target.value);
};
const handlePasswordChange = (e) => {
  setPassword(e.target.value);
  console.log(e.target.value);
};

// Creamos la variable para el boton:
const disabled = !(username && password);

// Tenemos los inputs:
<input
  type="text"
  name="username"
  id="username"
  onChange={handleUsernameChange}
/>
<input
  type="password"
  name="password"
  id="password"
  onChange={handlePasswordChange}
/>
<Button type="submit" $variant="primary" disabled={disabled}>
  Log in
</Button>
```

Lo que tenemos aqui es que podemos calcular el estado del boton a consecuencia de los estados de los valores de los inputs. No seria necesario poner un estado solo para el boton porque su estado depende de los valores de los inputs.

Con el evento onChange={} ya tenemos un poco más de control sobre nuestros inputs, para eso tenemos el atributo value={} que le podemos pasar a los inputs. El valor que le pasamos a ese atributo es el de estado para cada input:

## LoginPage.jsx:

```javascript
<input
  type="text"
  name="username"
  id="username"
  onChange={handleUsernameChange}
  value={username}
/>
<input
  type="password"
  name="password"
  id="password"
  onChange={handlePasswordChange}
  value={password}
/>
```

Como vemos username y password. Que son los valores de estado. Ahora si ya tenemos los inputs controlados. Tener un input controlado es pasarle la propiedad value={}. Pero OJO es necesario que el value={} siempre vaya de la mano del evento onChange={}, el unico que hace cambiar el value={} es el onChange={}.

## Checkbox y radio:

Para el caso de checkbox y radio:

En lugar de value(controlado) defaultValue(no controlado) se utiliza checked(controlado) y defaultChecked (no controlado) para mostrar la seleccion, esto lo vemos en el pdf de la clase:

```javascript
<input
  type="checkbox"
  checked={checked}
  onChange={(e) => {
    setChecked(e.target.value);
  }}
  name="check"
  id="check"
/>
```

Nuestro componente LoginPage.jsx con los inputs ya controlados nos queda asi:

## LoginPage.jsx:

```javascript
import { useState } from 'react';
import Button from '../../components/Button';
import { login } from './service';

const LoginPage = ({ onLogin }) => {
  // Estado para los imputs:
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  // Funcion handle del formulario
  const handleSubmit = async (e) => {
    // Evitamos que se envie el formulario por defecto:
    e.preventDefault();

    // Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
    await login({ username, password });

    // aqui el usuario ya esta logueado: Lanzamos el evento que viene del padre:
    onLogin();
  };

  // Creamos la funcion para los inputs controlados:
  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
    console.log(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
    console.log(e.target.value);
  };

  // Creamos la variable para el boton: esta linea de ejecuta en cada render
  const disabled = !(username && password);

  return (
    <div>
      <h1>Login in to X</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="username"
          id="username"
          onChange={handleUsernameChange}
          value={username}
        />
        <input
          type="password"
          name="password"
          id="password"
          onChange={handlePasswordChange}
          value={password}
        />

        <Button type="submit" $variant="primary" disabled={disabled}>
          Log in
        </Button>
      </form>
    </div>
  );
};

export default LoginPage;
```
