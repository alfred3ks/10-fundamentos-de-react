# Persistencia de datos:

Actualmente nuestra aplicacion se guarda en memoria, si actualizamos el navegador vemos que vuelve al login. Podemos ver tambien que estamos guardando en la cabecera al hacer el login el token que nos responde la api. Lo vemos en las cabeceras Authorization.

Tenemos que hacer persistencia del token. Lo tenemos que hacer en el cliente. La forma de hacerlo es guardar en localStorage ese token.

Guardaremos el token en localStorage. Este token estonces estara guardado en el cliente de axios y en localStorage.

Cuando nuestra aplicacion arranque lo que primero que hara es revisar el localStorage a ver si hay token si lo hay mostrara el listado que no hay mostrara formulario.

Luego aunque cerremos la aplicación, el navegador el token seguira ahi, ya depende del acuerdo de la api, muchas estos token suelen tener un tiempo de vida, dependera de la programación del backend.

Vamos a crear una utilidad para crear la funcionalidad del localStorage. Creamos una carpeta llamada utils. Nos creamos el fichero storage.js.

## utils/storage.js:

```javascript
const storage = {
  // método para obtener el local storage:
  get(key) {
    const value = localStorage.getItem(key);
    if (!value) {
      return null;
    }
    return JSON.parse(value);
  },

  // método para guardar el localstorage
  set(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  },

  // método para borrar el token:
  remove(key) {
    localStorage.removeItem(key);
  },

  // método para limpiar el localstorage
  clear() {
    localStorage.clear();
  },
};

export default storage;
```

Ahora en el servicio.js de LoginPage.jsx vamos a hacer esa persistencia:

## auth/service.js:

```javascript
import storage from '../../utils/storage';

export const login = (credentials) => {
  // Usamos el cliente se axios con el metodo post a la url pasandole las credenciales:
  return client.post('/auth/login', credentials).then(({ accessToken }) => {
    // Guardamos en axios el token:
    setAuthorizationHeader(accessToken);

    // Persistimos el token en el storage:
    storage.set('auth', accessToken);
  });
};
```

Nos toca ahora cada vez que carge la aplicacion vaya a local storage y cheque si hay un token. Si hay mostrara el listado no hay mostrara el login, eso lo tenemos que hacer en el punto de entrada de la aplicación. Lo hacemos en main.jsx:

## main.jsx:

```javascript
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import storage from './utils/storage.js';
import { setAuthorizationHeader } from './api/client.js';

// Revisamos si hay token:
const accedToken = storage.get('auth');

if (accedToken) {
  // guardamos en axios:
  setAuthorizationHeader(accedToken);
}

// Convertimos el token en booleano: Sera true o false:
const isUserLogged = !!accedToken;

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App initiallyLogged={isUserLogged} />
  </React.StrictMode>
);
```

Tambien seteamos el token a axios, se lo pasamos asi como hemos dicho al usarlo en futuras peticiones ese token ya estara guardado y axios lo enviara en la cabecera.

Cambiamos el token a booleano y se lo pasamos a <App/> para que vaya por props. Esa props la consumimos en el componente App.jsx:

## App.jsx:

```javascript
import { useState } from 'react';
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';

const App = ({ initiallyLogged }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);

  // declaramos nuestro evento que pasaremos por props a LoginPage:
  const handleLogin = () => setIsLogged(true);
  return (
    <div>
      {isLogged ? (
        <TweetsPage dark={false} />
      ) : (
        <LoginPage onLogin={handleLogin} />
      )}
    </div>
  );
};

export default App;
```

Como podemos ver el componente recibe por props initiallyLogged que sera true o false, y eso lo usamos para setear el estado. Lo probamos y funciona!!! Ya tenemos persistencia de datos, ahora al refrescar la pagina ya tenemos persistencia de datos.

Una cosa que es bueno saber que el token esta guardado en localStorage asociado al dominio.
