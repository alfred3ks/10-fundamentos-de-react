# React:

React es una libreria para construir interfaces de usuarios de una manera bastante sencilla usando javascript.

- Fue creada por facebook y liberada en el año 2013.
- Libreria de interfaces de usuario en el desarrollo de aplicaciones SPA (Single Page Applications).
- No es un framework, pero se ha creado un amplio ecosistema de librerias alrededor de ella (estado, routing, ajax, ...)
- Su documentacion la podemos encontrar en el siguiente enlace:

React: (https://es.react.dev/)

## Principios básicos de React:

Sigue tres principios básicos:

- Declarativo: diseñar la vista para cada estado, react actualiza de modo eficiente cuando cambian los datos.

- Basado en componentes: contruir componentes encapsulados y componer interfaces mas complejas.

- Aprender una vez, escribir todo tipo de aplicaciones:
  - React: libreria core con todo el nucle de funcionalidad,
  - React-DOM: aplicaciones web,
  - React-Native: aplicaciones móviles.

Ejemplo de un código declarativo: No nos preocupa como se obtiene el resultado, no hacemos incapie en el como sino en el que:

```javascript
const props = {
  name: 'KeepCoding';
}
function Welcome(props){
  return <h1>Hello, {props}</h1>
}
```

A continuación vamos a realizar unos ejemplo de muy bajo nivel para ver que e react.
