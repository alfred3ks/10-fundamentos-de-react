# React Fragment:

Un componente no puede devolver varios elementos de React a la vez (seria como tener varios retornos de createElement).

Podriamos envolver todo el retorno en un div, pero podriamos romper los estilos, o la semantica del HTML.

React nos ofrece el componente <Fragment></Fragment>:

Prodriamos usarlos asi:

```javascript
import { Fragment } from 'react';

const Header = ({ title, subtitle }) => {
  return (
    <Fragment>
      <h1>{title}</h1>
      <h2>{subtitle}</h2>
    </Fragment>
  );
};
```

Esta es otra opción: <></>

```javascript
import { Fragment } from 'react';

const Header = ({ title, subtitle }) => {
  return (
    <>
      <h1>{title}</h1>
      <h2>{subtitle}</h2>
    </>
  );
};
```

## react-fragment.html:

Vemos dos formas de usar fragment para renderizar dos componentes:

```javascript
reactRoot.render(
  <React.Fragment>
    <Counter />
    <Counter />
  </React.Fragment>
);

reactRoot.render(
  <>
    <Counter />
    <Counter />
  </>
);
```
