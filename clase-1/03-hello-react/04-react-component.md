# React component:

Hasta ahora lo que hemos creados son elementos de react. Mis aplicaciones de react son un conjunto de piezas de elementos de react.

Tenemos que tener claro lo que es un elemento y lo que es un componente. Un elemento es todo aquello que react transforma en un trozo de interface.

En React, los términos "componente" y "elemento" se utilizan para referirse a conceptos distintos, aunque están relacionados.

## Componente:

Un componente en React es una pieza reutilizable y modular de código que encapsula la lógica y la interfaz de usuario de una parte específica de la aplicación.
Puede ser una función o una clase que acepta ciertas propiedades (props) y devuelve elementos React que representan la interfaz de usuario.
Los componentes pueden tener estado interno y pueden gestionar su propio ciclo de vida.

## Elemento:

Un elemento en React es una descripción ligera de lo que se debe renderizar en el DOM. Es la unidad más pequeña de la interfaz de usuario en React.
Puede ser un componente, pero también puede ser un elemento primitivo de HTML (como un <div>, <span>, etc.).
Los elementos son los bloques de construcción básicos de las aplicaciones React y se crean utilizando la función React.createElement o utilizando JSX (que es una sintaxis más cómoda).

En resumen, un componente es una abstracción más grande que encapsula la lógica y la interfaz de usuario, mientras que un elemento es la unidad más pequeña y es la representación de lo que realmente se renderizará en el DOM. Los componentes pueden estar compuestos por elementos y otros componentes.

Un componente renderiza en linea generales a elementos.

## 04-react-component:

```javascript
// Creamos un componente: Reutlizamos código:
const Message = (props) => {
  // Destructuring
  const { className, children } = props;
  return <span className={className}>{children}</span>;
};

const element = (
  <div className="container">
    <Message className="hello">Hello KeepCoding!!</Message>
    <Message className="bye">Bye KeepCoding!!</Message>
    <Message className="third">
      <span>
        I'am a <strong>span</strong>
      </span>
    </Message>
  </div>
);
```
