# React Counter State:

Vamos a ver un ejemplo como podemos manejar el estado de un componente. tambien veremos un poc de entrada los eventos. Estos eventos en react no son mas que atributos de tipo funcion.

Cuando tenemos un evento este lo podemos capturar, en los elementos nativos tenemos el evento->(e), en react se llama syntheticBaseEvent->(e). Es un evento un poco mas potente que envuelve evento nativo.

Para los evento cuando usemos el onClick={}, lo correcto es pasarle siempre la funcion sin ejecutar, no pasarla con los parentesis de ejecucion:

## Correcto:

Vemos la funcion clickHandle, cuando se la pasamos al evento onClick se la pasamos sin los parentesis de ejecución:

```javascript
// Creamos un componente Counter:
const Counter = () => {
  const counter = 0;

  const clickHandler = (e) => {
    e.preventDefault();
    console.log(e);
  };

  return (
    <div>
      <p>{counter}</p>
      <button onClick={clickHandler}>Click me!!</button>
    </div>
  );
};
```

## InCorrecto:

```javascript
// Creamos un componente Counter:
const Counter = () => {
  const counter = 0;

  const clickHandler = (e) => {
    e.preventDefault();
    console.log(e);
  };

  return (
    <div>
      <p>{counter}</p>
      <button onClick={clickHandler()}>Click me!!</button>
    </div>
  );
};
```

## Cambio de estado del contador: State:

Para poder cambiar el estado de un componente necesitamos introducir unas funciones especiales de react llamadas hooks, existe un hook para el tema del estado llamado useState().

```javascript
// Creamos un componente Counter:
const Counter = () => {
  // declaramos el hook useState()
  // const state = React.useState(0);
  /*
  La variable state es un arreglo de 2 posiciones, donde en la primera posicion es el valor inicial y la segunda una funcion que al ejecutarla puede cambiar el valor inicial.
  */
  // Hacemos destructuring del arreglo:
  const [counter, setCounter] = React.useState(0);

  const clickHandler = (e) => {
    console.log(e);
    e.preventDefault();
    setCounter(counter + 1);
  };

  return (
    <div>
      <button onClick={clickHandler}>Click me!!</button>
      <span>{counter}</span>
    </div>
  );
};
```

## Recomendacion:

Existe otra forma de usar la funcion setCount para los estado, es recomendado hacerlo asi:

```javascript
const clickHandler = (e) => {
  console.log(e);
  e.preventDefault();
  // setCounter(counter + 1);
  setCounter((v) => {
    return v + 1;
  });
};
```

Como vemos a la funcion setCounter(), le pasamos una funcion que recibe por parametro un valor inicial y luego retornamos ese valor inicial con el incremento. Aqui lo que hacemos es hacer incrementando 1 al valor que tengas. Al ultimo que tengas anteriormente. recomendacion hacerlo asi cuando el estado que deseemos genrar dependa del estado anterior usar esta forma.
