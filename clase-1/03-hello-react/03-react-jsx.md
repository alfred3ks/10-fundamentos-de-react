# React JSX:

Escribir React usando createElement es bastante engorroso, no practico. Sobre todo si queremos anidar elementos unos dentro de otros. Para eso se creo JSX. Este nos permite createElement que parece html.

Para poder usar jsx en el navegador y no nos de warning la consola de que el navegador no entiendo ese html que le pasamos como jsx necesitamos usar babel para poder hacer la transpilacion de código. Babel hara todo el trabajo de createElement por debajo coje el JSX y lo lleva a createElement para que lo entienda el navegador.

La "transpilación" es un término que se utiliza para describir el proceso de convertir el código fuente de un lenguaje de programación a otro de manera que el código resultante sea equivalente en funcionalidad, pero esté escrito en un lenguaje diferente. A diferencia de la compilación, donde se traduce el código fuente a un código de máquina específico, la transpilación generalmente implica la traducción de código fuente a otro lenguaje de nivel superior.

Para esto debemos importar un script de babel:

```javascript
<script
  crossorigin
  src="https://unpkg.com/@babel/standalone@7.8.3/babel.min.js"
></script>
```

Tambien debemos ahora módificar nuestro script donde ejecutaremos el código js:

```javascript
<script type="text/babel"></script>
```

## 03-react-jsx.html:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>React</title>
  </head>
  <body>
    <div id="root"></div>

    <!-- Importamos las librerias por medio de cdn de react y react-dom -->
    <script
      crossorigin
      src="https://unpkg.com/react@18/umd/react.development.js"
    ></script>
    <script
      crossorigin
      src="https://unpkg.com/react-dom@18/umd/react-dom.development.js"
    ></script>
    `

    <!-- Importamos el script para babel -->
    <script
      crossorigin
      src="https://unpkg.com/@babel/standalone@7.8.3/babel.min.js"
    ></script>

    <script type="text/babel">
      // Obtenemos el div del DOM:
      const rootElement = document.getElementById('root');

      /*
    Creamos un objeto usando JSX:
    */
      const element = (
        <div className="container" id="container">
          <span>Hello World!</span>
        </div>
      );

      console.log(element); // Nos crea un objeto {}, ver en el navegador:

      // Usamos React DOM para meter el objeto al DOM: Este es el encargado de hacer la transformación:
      ReactDOM.createRoot(rootElement).render(element);
    </script>
  </body>
</html>
```

## Interpolar JS en JSX:

En JSX tenemos algo que son puertas a JS. Podemos interpolar código JS en el JSX por medio de las llaves {}. Creamos las variables o nos viene de una api de donde sea y la interpolamos en en JSX. Dentro de los {} podemos meter cualquier expresion de javascript que retornen un resultado. No podemos usar condicionales. Tampoco ciclos, estos no son expresiones que retornen algo. Null no se rendiriza. Los booleanos tampoco se renderizan.

Para renderizar objetos tampoco podriamos nos daria un fallo. Para poderlo mostrar un objeto lo tendriamos que transformar a string.

Para el tema de las clases para estilos no podemos usar class por que es una palabra reservada del lenguaje usaremos className en camelcase. Tambien pasa con el for usaremos htmlFor como atributo.

Podemos pasar como atributos tambien eventos.

Tambien podemos renderizar expresiones de tipo suma. a continuacion vemos como podemos hacerlo:

```javascript
/*
Creamos un objeto usando JSX:
*/
const className = 'container';
const id = 'container';

const getContent = () => 'Hello World!!';

const number = {
  a: 1,
  b: 2,
  c: 3,
};
const element = (
  <div
    className={className}
    id={id}
    onClick={() => {
      console.log('click');
    }}
  >
    {/* Asi seria un comentario en JSX */}
    <span>{getContent()}</span>
    <div>{5 + 5}</div>
    <div>{`${getContent()} en React`}</div>
    <div>Bey</div>
    <div>{null}</div>
    <div>{true}</div>
    <div>{false}</div>
    <div>{JSON.stringify(number)}</div>
  </div>
);
```

Podriamos organizar todos los atributos que le pasemos a nuestro JSX y luego pasarlos como props asi usando el operador spread {...}, Asi tendremos una sintaxis mas limpia:

```javascript
/*
Creamos un objeto usando JSX:
*/

const divProps = {
  className: className,
  id: id,
  onClick: () => console.log('click'),
};

const element = <div {...divProps}></div>;
```
