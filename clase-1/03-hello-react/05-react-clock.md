# React clock:

Vamos a ver como funciona react y react DOM. Vamos a renderizar por pantalla un reloj.

Con este ejemplo lo que hacemos es ver como react solo produce cambio en el dom de aquellos elementos que cambien, si un elemento no cambia el no genera nada sobre el dom. Lo podemos ver en el inspector de elementos, en el html. Lo que hace react con virtual dom impactar lo menos posible en el dom y eso ayuda mucho en el performance de una aplicación.
