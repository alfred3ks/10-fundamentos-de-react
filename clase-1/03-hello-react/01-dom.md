# Hello React:

Para arrancar los ejemplos usaremos este script de npx:

```bash
npx browser-sync start --server --files "./*.html" --no-notify --directory
```

En la carpeta hello-react vamos a crear los ejemplos mas sencillos que podemos hacer con react.

## 01-dom.html:

En este ejercicio lo que vemos es como crear elementos con javascript vanila y agregarlos al DOM:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div id="root"></div>

    <script type="text/javascript">
      // Obtenemos el div del DOM:
      const rootElement = document.getElementById('root');

      // Creamos el elemento:
      const element = document.createElement('div');
      element.textContent = 'Hello World!';
      element.className = 'container';

      // Agregamos el elemento al dom:
      rootElement.appendChild(element);
    </script>
  </body>
</html>
```
