# Manejo del error en el componente TweetPage.jsx:

De momento en el componente TweetPage si vamos a un tweet que existe este se muestra pero si por url le pasamos uno que no existe no redirecciona al 404, debemos en el cleinte manejar eso:

## api/client.js:

```javascript
// Creamos un interceptor de la respuesta para sacar el data y manejamos las respuestas de error de la api:
client.interceptors.response.use(
  (response) => response.data,
  (error) => {
    console.log(error);
    if (error.response) {
      // 400 / 500 server error: pro aqui errores del servidor un {}
      return Promise.reject({
        message: error.response.statusText,
        ...error.response,
        ...error.response.data,
      });
    }
    // Por aqui errores de network, Request error, sin conexion a internet: un {}
    return Promise.reject({ message: error.message });
  }
);
```

Como vemos hemos agregado al interceptor un spread del ...error.response y ahora si funciona.

## TweetPage.jsx:

```javascript
// Hacemos la peticion al servidor:
useEffect(() => {
  getTweet(id)
    .then((tweet) => setTweet(tweet))
    .catch((error) => {
      if (error.status === 404) {
        console.log('entro');
        navigate('/404');
      }
    });
}, [navigate, id]);
```
