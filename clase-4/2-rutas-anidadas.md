# Rutas anidadas:

Como vemos el Laylok se repite en varias rutas, lo que haremos es crear rutas anidadas para hacer el Layout fijo el cual es común.

Para esto tenemos el componente <Outlet/>, lo vimo en la información de componentes principales.

Para nuestro caso la ruta /tweets va a tener tres subrutas.

/tweets
/tweets/:id
/tweets/new

## App.jsx:

```javascript
{
  /* Rutas anidadas */
}
<Route path="/tweets" element={<Layout />}>
  <Route index element={<TweetsPage />} />
  {/* Ruta dinamica */}
  <Route path=":id" element={<TweetPage />} />
  <Route path="new" element={<NewTweetPage />} />
</Route>;
```

Como se puede ver tenemos la ruta padre /tweets que renderiza al Layout como element. Dentro de Route tenemos las rutas, la primera ruta que corresponde /tweets es la que lleva el atributo index. Luego en nuestro componente Layout recibe tanto el children como el componente Outlet de React Router.

## Layout.jsx:

```javascript
import { Outlet } from 'react-router-dom';

// Rutas anidadas: Renderizamos el componente Outles de React Router
const Layout = ({ children }) => {
  return (
    <div className="layout">
      <Header className="layout-header bordered" />
      <main className="layout-main bordered">
        {children ? children : <Outlet />}
      </main>
      <Footer className="layout-footer bordered">KeepCoding</Footer>
    </div>
  );
};
```

Cuando usamos rutas anidadas y pasamos dentro de un componente Route otro Route como vemos arriba vemos que podemos usar el componente Outlet de React router para renderizar los hijos, es como el children solo que React Router lo llama Outlet. Este componente solo lo usamos para rutas anidadas.
