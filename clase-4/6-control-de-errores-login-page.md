# Control de errores LoginPage.jsx:

Vamos a controlar los distintos errores y estados de nuestra app. Vamos a empezar con el login en el componente loginPage.jsx:

Vamos a controlar cuando el usuario mete unas credenciales que no son validas. Cuando eso pasa la api nos retorna un 401. Tambien podemos manejar si no hay conexion con api. El punto para controlar esto lo tenemos en la llamada al servicio. Lo podemos ver en api/client.js.

## api/client.js:

```javascript
// Creamos un interceptor de la respuesta para sacar el data y manejamos las respuestas de error de la api:
client.interceptors.response.use(
  (response) => response.data,
  (error) => {
    console.log(error);
    if (error.response) {
      // 400 / 500 server error: pro aqui errores del servidor un {}
      return Promise.reject({
        message: error.response.statusText,
        ...error.response.data,
      });
    }
    // Por aqui errores de network, Request error, sin conexion a internet: un {}
    return Promise.reject({ message: error.message });
  }
);
```

Ahora en loginPage.jsx vamos a manejar esos errores para mostrarlos al usuario, creamos un estado que lo capturamos por medio de trycach y lo mostramos en el formulario.

## LoginPage.jsx:

```javascript
// Creamos el estado para el error:
const [error, setError] = useState(null);

// Funcion handle del formulario
const handleSubmit = async (e) => {
  // Evitamos que se envie el formulario por defecto:
  e.preventDefault();

  // Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
  try {
    await login(credentials);
    // aqui el usuario ya esta logueado: Lanzamos el evento que viene del padre:
    onLogin();

    // Si tenemos algo en location redireccionamos o vamos al home:
    const to = location?.state?.from?.pathname || '/';
    navigate(to, { replace: true });
  } catch (error) {
    setError(error);
    // Establecer un temporizador para limpiar el error después de 5 segundos
    setTimeout(() => {
      setError(null);
      }, 5000);
    }
  }
};
{
  /* Mostramos error */
}
{
  error && <div className="loginPage-error">{error.message}</div>;
}
```

## Control de velocidades lentas boton de login:

Ahora lo que haremos es introducir para conexiones lentas un cargando que lo pondremos en el boton, cuando el usuario haga clicl en el boton de login se quitara la palabra login y se mostrara cargando...

Para esto crearemos otro estado:

## LoginPage.jsx:

```javascript
// Creamos el estado para conexiones lentas:
const [isFeching, setIsFeching] = useState(false);

// Funcion handle del formulario
const handleSubmit = async (e) => {
  // Evitamos que se envie el formulario por defecto:
  e.preventDefault();

  // Usamos el metodo del servicio, le pasamos los datos {} del estado, tenemos nosotros el control:
  try {
    setIsFeching(true);
    await login(credentials);
    setIsFeching(false);
    // aqui el usuario ya esta logueado: Lanzamos el evento que viene del padre:
    onLogin();

    // Si tenemos algo en location redireccionamos o vamos al home:
    const to = location?.state?.from?.pathname || '/';
    navigate(to, { replace: true });
  } catch (error) {
    setIsFeching(false);
    setError(error);
    // Establecer un temporizador para limpiar el error después de 5 segundos
    setTimeout(() => {
      setError(null);
    }, 5000);
  }
};

// Hacemos destructuring de las credenciales:
const { username, password } = credentials;
// Creamos la variable para el boton: esta linea de ejecuta en cada render
const disabled = !(username && password) || isFeching;

{
  /* Mostramos el boton de cargando al hacer Login con estado */
}
<Button
  className="loginForm-submit"
  type="submit"
  $variant="primary"
  disabled={disabled}
>
  {!isFeching ? 'Login' : 'Loading...'}
</Button>;
```

De esta forma ya tenemos el boton que mostrara cargando y hasta que acabe la petición no se habilitara y le dara fittback al usuario que se esta enviando la peticion. Sobre todo en conexiones lentas.
