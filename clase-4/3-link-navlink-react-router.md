# Link, NavLink de React Router:

Vamos a ver el tema de los enlaces para poder ir a cada una de las rutas antes creadas. Para eso tenemos dos componentes, <Link/> y <NavLink/>

Vamos a poner en el menu el enlace a crear un nuevo tweet, y tambien que al pulsar el logo nos lleve a la home del sitio, eso lo tenemos que hacer en Header.jsx, tambien cuando hagamos click en cada tweet nos mande al detalle corespondiente de cada uno:

## Header.jsx: Logo y enlace a crear tweet:

```javascript
import { Link, NavLink } from 'react-router-dom';

const Header = ({ className }) => {
  return (
    <header className={clsx('header', className)}>
      <div className="header-logo">
        <Link to={'/'}>
          <Icon width={32} fill="crimson" />
        </Link>
      </div>

      <nav className="header-nav">
        <NavLink to={'/tweets/new'}>Create tweet</NavLink> |
        <NavLink to={'/tweets'} end>
          Lastest tweets
        </NavLink>
        <AuthButton className={'header-button'} />
      </nav>
    </header>
  );
};
```

## TweetPage.jsx: Enlace al componente del detalle del tweet:

```javascript
import { Link } from 'react-router-dom';

<ul>
  {tweets?.map(({ id, ...tweet }) => (
    <li key={id}>
      {/* {Ruta absoluta} */}
      {/* <Link to={`/tweets/${id}`}> */}
      {/* Ruta relativa */}
      <Link to={`${id}`}>
        <Tweet {...tweet} />
      </Link>
    </li>
  ))}
</ul>;
```

El componente NavLink funciona igual que el componente Link pero este componente sabe cual es la ruta activa y la pinta con los estilos. Lo que hace es que machea la ruta con el enlace y le coloca la clase active. Si en nuestro css tenemos creada esa clase con un color se le asignara ese color. Como machea la ruta con lo que hemos pulsado debemos usar el atributo end para decirle que machee solo esa ruta que termina ahi.

## Header.css:

```css
.header-nav .active {
  color: rgba(29, 161, 242, 1);
}
```

Podemos cambiar esa clase .active, si no queremos usar esa, lo podemos cambiar le pasamos al NavLink una className de tipo funcion:

## Header.jsx:

```javascript
<nav className="header-nav">
  <NavLink
    to={'/tweets/new'}
    className={(isActive) => (isActive ? 'selected' : '')}
  >
    Create tweet
  </NavLink>
  |<NavLink to={'/tweets'} end>
    Lastest tweets
  </NavLink>
  <AuthButton className={'header-button'} />
</nav>
```

Aunque en general solemos usar como vimos arriba, esto mejor ver mejor la documentacion en React Router.

Ahora tambien vamos a cambiar el boton de Logout usando el navlink. Pero queremos que cuando hagamos Logout ese boton que aparece sea un Link que nos envie al login, porque ahora no lo hace solo se muestra, eso lo tenemos que hacer en el componente AutButton.jsx:

Aqui entra en juego styled component que permite eso.

## AuthButon.jsx:

```javascript
import { Link } from 'react-router-dom';

<Button as={Link} to={'/login'} $variant="primary" className={className}>
  Login
</Button>;
```

Ya tenemos listo todo la parte estatica, falta la parte programatica. Coo por ejemplo proteger rutas, mostrar ciertas rutas o paginas si el usuario esta logueado.

NOTA: Todos los componentes Link tienen una propiedad replace, esta propiedad significa que si la ponemos no inroducimos una nueva entrada en el history del navegador, sino sustituye la actual. Esto es muy interesante por si no queremos q vuelvan atras si el usuario quiere.
