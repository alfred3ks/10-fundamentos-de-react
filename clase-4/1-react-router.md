# React Router:

Libreria externa no react. Osea que debemos instalar. Es una de las mas usadas. Si manejamos rutas necesitamos a React Router.

Tenemos las url a la documentación:

https://www.npmjs.com/package/react-router-dom

https://reactrouter.com/en/main

## Principios básicos:

- Server side routing:

  - Browser solicta una ruta al servidor, descarga una respuesta HTML con JS y CSS y renderiza esa respuesta.
  - Cuando el usuario hace click en un enlace, el proceso empieza de nuevo.

- Client side routing:

  - Cuando el usuario hace click en un enlace, la URL del browser se actualiza sin hacer una nueva petición al servidor.
  - Un componente descargado previamente en el JS, el router, se encarga de sincronizar la interfaz mostrada con la URL del browser, es decir, mostrar unos componentes React u otros, en función de la la URL.
  - React Router nos ofrece una serie de utilizades y componentes React que se encargan de este client side routing.

Vamos a instalarlo en nuestro proyecto de X:

```bash
npm i react-router-dom
```

Vamos a envolver todo nuestra aplicación por medio del componente <BrowserRouter></BrowserRouter> que nos proporciona la libreria, lo harems en main.jsx:

## main.jsx:

```javascript
// Importamos componente de React Router:
import { BrowserRouter } from 'react-router-dom';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <AuthContextProvider initiallyLogged={isUserLogged}>
        <App />
      </AuthContextProvider>
    </BrowserRouter>
  </React.StrictMode>
);
```

Ya con esto estamos dotando nuestra aplicación de un concepto de routing.

## Componentes principales:

- Route: Componente que asocia una ruta con el elemento React.
- Routes: Agrupación de componentes Route. Decide que ruta dentro de la agrupación ofrece el mejor match.
- Outlet: En rutas anidadas, es usado en la ruta padre para renderizar la ruta hija en su lugar. Una especie de hueco que el padre deja para ser sustituido por el resultado de la ruta hija.
- Navigate: Componente que cuando es renderizado cambia la URL. Es como una redirección declarativa.
- Link: Enlace que permite la navegación a otro URL.
- NavLink: Como Link, pero este componente 'sabe' cual es la ruta activa y puede aplicar estilos basado en ello, muy util en un menu de la aplicación.

Vamos a crear las rutas, eso lo haremos en App.jsx.

## App.jsx

```javascript
import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';
import TweetPage from './pages/tweets/TweetPage';
import NewTweetPage from './pages/tweets/NewTweetPage';
import Layout from './components/layout/Layout';
// Importamos los componentes de React Router:
import { Routes, Route, Navigate } from 'react-router-dom';

const App = () => {
  return (
    <Routes>
      <Route path="/login" element={<LoginPage />} />
      <Route
        path="/tweets"
        element={
          <Layout>
            <TweetsPage />
          </Layout>
        }
      />
      {/* Ruta dinamica */}
      <Route
        path="/tweets/:id"
        element={
          <Layout>
            <TweetPage />
          </Layout>
        }
      />
      <Route
        path="/tweets/new"
        element={
          <Layout>
            <NewTweetPage />
          </Layout>
        }
      />

      {/* Redireccion ya que en '/' no tenemos nada */}
      <Route path="/" element={<Navigate to="/tweets" />} />

      {/* con el * le decimos que es cualquier ruta que no hemos tratado */}
      <Route
        path="/404"
        element={
          <Layout>
            <div>404 not fount</div>
          </Layout>
        }
      />
      <Route path="*" element={<Navigate to="/404" />} />
    </Routes>
  );
};
```

Ya tenemos una primera versión de las rutas. Las probamos en el navegador y vemos que funcionan.
