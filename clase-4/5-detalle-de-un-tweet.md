# Detalle de un tweet:

Lo que vamos a hacer es mostrar el detalle de un tweet en el componente, como podemos ver ahora no estamos mostrando nada es el mismo componente, lo que haremos es capturar de la url el id del tweet y mostrar ese tweet buscando con la api en la bd. Esto lo veremos en el componente TweetPage.jsx:

Para esto debemos crear en el servicio un método para hacer la peticion a la api y traernos la informacion:

## service.js:

```javascript
// Método para solicitar tweet página detalle:
export const getTweet = (tweetId) => {
  const url = `${tweetsUrl}/${tweetId}`;
  // Hacemos una peticion:
  return client.get(url);
};
```

## TweetPage.jsx:

```javascript
import { useEffect, useState } from 'react';
import Content from '../../../components/layout/Content';
import { useParams, useNavigate } from 'react-router-dom';

import { getTweet } from '../service';

const TweetPage = () => {
  // Guardamos la respuesta en un estado:
  const [tweet, setTweet] = useState(null);
  const navigate = useNavigate();
  // const params = useParams();

  const { id } = useParams();
  // console.log(id);

  // Hacemos la peticion al servidor:
  useEffect(() => {
    getTweet(id)
      .then((tweet) => setTweet(tweet))
      .catch((err) => {
        if (err.status === 404) {
          navigate('/404');
        }
      });
  }, [navigate, id]);

  return (
    <Content title="Tweet detail">
      <div>
        Tweet detail {id} goes here...
        <div>{tweet && <code>{JSON.stringify(tweet)}</code>}</div>
      </div>
    </Content>
  );
};

export default TweetPage;
```
