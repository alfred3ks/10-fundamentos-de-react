# Protección de rutas:

# Hooks de React Router:

React Router tambien nos da unos hooks que podemos usar para ciertos casos. Estos hooks nos permiten hacer cosas de navegacion por código.

Los hooks son los siguientes:

useParams: Devuelve un onjeto {} con los parametros dinámicos:

```javascript
const { id } = useParams();
```

useLocation: Devuelve el objeto location actual:

```javascript
const location = useLocation();
```

useNavigate: Devuelve una función con la que podremos realizar redirecciones imperativas(por código):

```javascript
const navifgate = useNavigate();
navigate('/login', options);
```

useSearchParams: Hook para leer y modificar la query string de la URL:

```javascript
const [useSearchParams, setSearchParams] = useSearchParams();
```

Veremos los tres primeros.

Vamos a proteger las rutas, si el usuario no esta logueado le vamos a redirigir al login.

## Protección de rutas:

Ir al login si no esta logueado si desea crear un tweet. Se loguea y le mandamos de donde viene.

Para proteger rutas React Router no nos proporciona ningun componente, vamos a crearnos uno nosotros, lo haremos en la carpeta components, todo lo que envolvamos con este componente estara protegido por la autenticacion.

## RequireAuth.jsx:

```javascript
import { useAuth } from '../../pages/auth/context';
import { Navigate } from 'react-router-dom';

const RequireAuth = ({ children }) => {
  const { isLogged } = useAuth();

  return isLogged ? children : <Navigate to={'/login'} />;
};

export default RequireAuth;
```

Ahora este componente lo usamos en la ruta que queremos proteger en este caso la de crear un tweet que la tenemos en App.jsx:

## App.jsx:

```javascript
import RequireAuth from './components/RequireAuth';

{
  /* Protegemos la ruta con nuestro componente */
}
<Route
  path="new"
  element={
    <RequireAuth>
      <NewTweetPage />
    </RequireAuth>
  }
/>;
```

Ahora nos queda que al loguearnos que le mande de donde viene. En este caso para crear un tweet que es la que esta protegida. Para eso en nuestro componente de proteccion de ruta le tenemos que pasar un objeto que suele llamarse state={}, ese objeto le vamos a guardar la location, que es el path de donde estaba.

## RequireAuth.jsx:

```javascript
import { useAuth } from '../../pages/auth/context';
import { Navigate, useLocation } from 'react-router-dom';

const RequireAuth = ({ children }) => {
  const { isLogged } = useAuth();

  // Hook para saber la location:
  const location = useLocation();

  return isLogged ? (
    children
  ) : (
    <Navigate to={'/login'} state={{ from: location }} />
  );
};

export default RequireAuth;
```

Con ese objeto state recordamos de donde viene, en este caso la ruta es /tweets/new, lo podemos ver si hacemos un console.log(location). Vale ya lo tenemos guardado el usuario hara login y le diremos que vaya a lo que tenemos guardado en location.

```javascript
<Navigate to={'/login'} state={{ from: location }} />
```

Vamos al componente LoginPage.jsx:

## LoginPage.jsx:

```javascript
import { useLocation, useNavigate } from 'react-router-dom';

// accedemos al objeto location de react router:
const location = useLocation();
// Usamos el hook para navegar:
const navigate = useNavigate();

// Si tenemos algo en location redireccionamos sino al home:
const to = location?.state?.from || '/';
navigate(to, { replace: true });
```

Ya con esto vemos como podemos hacer el login y nos redireciona al home pero si deseamos crear un tweet y no esta loguedo le manda la login y vuelve de donde venia al loguearse.

Cuando ejecutamos la funcion navigate(), le pasamos como segundo parametro un objeto donde le decimos que la propiedad replace sea true, navigate(to, { replace: true }); eso lo que evita que cuando el usuario se loguee si le da a la flecha de volver atras no vuelve al login porque ya esta logueado.
