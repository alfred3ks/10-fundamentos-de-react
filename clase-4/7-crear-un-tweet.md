# Crear un tweet:

Vamos a hacer el flujo para crear un tweet y controlar los errores. Vamos a trabajar en el comoponente NewTweetPage.jsx.

Vamos a definir un contador de palabras como maximo 140 palabras.

Vamos al servicio y vamos a crear el método para poder hacer un post del tweet a la api:

## service.js:

```javascript
// Método para crear un nuevo tweet:
export const createTweet = (tweet) => {
  const url = tweetsUrl;
  return client.post(url, tweet);
};
```

Ahora es nuestro componente vamos a crear todo la lógica para mostrar numero de caracteres/maximo de caracteres, y desabilitar el boton para un minumo de caracteres:

## NewTweetPage.jsx:

```javascript
import { useState } from 'react';

// Declaramos las variables para los caracteres del text area
const MIN_CHARACTERES = 5;
const MAX_CHARACTERES = 140;

// Estado para almacenar lo que teclea el usuario:
const [content, setContent] = useState('');

// creamos la funcion hangle para el textarea:
const handleChange = (e) => {
  setContent(e.target.value);
};

// mostramos el maximo de caracteres:
const characteres = `${content.length} / ${MAX_CHARACTERES}`;

// Desabilitamos el boton para este minimo de caracteres:
const buttonDisabled = content.length <= MIN_CHARACTERES;

<Textarea
  className="newTweetPage-textarea"
  placeholder="Hey! What's up!"
  value={content}
  onChange={handleChange}
  maxLength={MAX_CHARACTERES}
/>;

<span className="newTweetPage-characters">{characteres}</span>;

<Button
  type="submit"
  className="newTweetPage-submit"
  $variant="primary"
  disabled={buttonDisabled}
>
```

Ahora vamos a enviar los datos, para eso tenemos el formaulario, agregamos su respectivo evento:

```javascript
import { useState } from 'react';
import { createTweet } from '../service';
import { useNavigate } from 'react-router-dom';

// Estado para manejar el fetching:
const [isFetching, setIsFetching] = useState(false);

// usamos navigate:
const navigate = useNavigate();

// Creamos la funcion para enviar los datos a la api:
const handleSubmit = async (e) => {
  e.preventDefault();
  try {
    setIsFetching(true);
    // Mandamos un objeto content porque es lo que espera la api:
    const tweet = await createTweet({ content });
    setIsFetching(false);
    // Hacemos una navegacion despues de crear el tweet:
    navigate(`../${tweet.id}`, { relative: 'path' });
  } catch (error) {
    if (error.status === '401') {
      navigate('/login');
    }
    setIsFetching(false);
  }
};

// desabilitamos el boton para este minimo de caracteres:
const buttonDisabled = content.length <= MIN_CHARACTERES || isFetching;

<form onSubmit={handleSubmit}></form>;
```

Lo probamos y funciona!!!!!

Podriamos agregarle tambien en el boton de enviar el estado como cuando hacemos login para conexiones lentas.

```javascript
{
  !isFetching ? 'Let`s go!' : 'Sending...';
}
```

Ahora lo que hacemos es manejar el error para mostrar una pastilla como en el login por ejemplo que no hay conexion con la api u otro error.

Le he agregado eun estado para el error, he colocado un setTimeout para borrar las pastilla pasado un tiempo y he colocado en el formulario el div con el error y su respectiva clase:

```javascript
// Creamos el estado para el error:
const [error, setError] = useState(null);

// Creamos la funcion para enviar los datos a la api:
const handleSubmit = async (e) => {
  e.preventDefault();
  try {
    setIsFetching(true);
    // Mandamos un objeto content porque es lo que espera la api:
    const tweet = await createTweet({ content });
    setIsFetching(false);
    // Hacemos una navegacion despues de crear el tweet:
    navigate(`../${tweet.id}`, { relative: 'path' });
  } catch (error) {
    if (error.status === '401') {
      navigate('/login');
    } else {
      setIsFetching(false);
      // mostramos un error si no hay conexion con la api:
      setError(error);
      // Establecer un temporizador para limpiar el error después de 5 segundos
      setTimeout(() => {
        setError(null);
      }, 5000);
    }
  }
};

{
  /* Mostramos error */
}
{
  error && <div className="newTweetPage-error">{error.message}</div>;
}
```

Tenemos el css:

## NewTweetPage.css:

```css
.newTweetPage-error {
  width: calc(100% - 30px);
  margin: 0 auto;
  background-color: rgb(224, 36, 94, 0.1);
  border: 1px solid rgba(224, 36, 94, 0.2);
  border-radius: 5px;
  color: rgb(224, 36, 94);
  margin-top: 60px;
  padding: 10px 15px;
}
```

Como hemos visto esto es un patron, lo de manejar los errores, debemos saber que tipo de errores nos puede retornar la api y eso manejarlo nosotros para mostrar al usuario que sucede, lo vimos en login y lo vemos en este componente de creacion de tweet.
