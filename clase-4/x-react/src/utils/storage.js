const storage = {
  // étodo para obtener el local storage:
  get(key) {
    const value = localStorage.getItem(key);
    if (!value) {
      return null;
    }
    return JSON.parse(value);
  },

  // método para guardar el localstorage
  set(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  },

  // método para borrar el token:
  remove(key) {
    localStorage.removeItem(key);
  },

  // método para limpiar el localstorage
  clear() {
    localStorage.clear();
  },
};

export default storage;
