import { useState } from 'react';
import Content from '../../../components/layout/Content';
import Button from '../../../components/shared/Button';
import Photo from '../../../components/shared/Photo';
import Textarea from '../../../components/shared/TextArea';

import { createTweet } from '../service';
import { useNavigate } from 'react-router-dom';

import './NewTweetPage.css';

// Declaramos las variables para los caracteres del text area
const MIN_CHARACTERES = 5;
const MAX_CHARACTERES = 140;

const NewTweetPage = () => {
  // Estado para almacenar lo que teclea el usuario:
  const [content, setContent] = useState('');

  // Estado para manejar el fetching:
  const [isFetching, setIsFetching] = useState(false);

  // Creamos el estado para el error:
  const [error, setError] = useState(null);

  // usamos navigate:
  const navigate = useNavigate();

  // creamos la funcion hangle para el textarea:
  const handleChange = (e) => {
    setContent(e.target.value);
  };

  // Creamos la funcion para enviar los datos a la api:
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setIsFetching(true);
      // Mandamos un objeto content porque es lo que espera la api:
      const tweet = await createTweet({ content });
      setIsFetching(false);
      // Hacemos una navegacion despues de crear el tweet:
      navigate(`../${tweet.id}`, { relative: 'path' });
    } catch (error) {
      if (error.status === '401') {
        navigate('/login');
      } else {
        setIsFetching(false);
        // mostramos un error si no hay conexion con la api:
        setError(error);
        // Establecer un temporizador para limpiar el error después de 5 segundos
        setTimeout(() => {
          setError(null);
        }, 5000);
      }
    }
  };

  // mostramos el maximo de caracteres:
  const characteres = `${content.length} / ${MAX_CHARACTERES}`;

  // desabilitamos el boton para este minimo de caracteres:
  const buttonDisabled = content.length <= MIN_CHARACTERES || isFetching;

  return (
    <Content title="What are you thinking?">
      <div className="newTweetPage">
        <div className="left">
          <Photo />
        </div>
        <div className="right">
          <form onSubmit={handleSubmit}>
            <Textarea
              className="newTweetPage-textarea"
              placeholder="Hey! What's up!"
              value={content}
              onChange={handleChange}
              maxLength={MAX_CHARACTERES}
            />
            <div className="newTweetPage-footer">
              <span className="newTweetPage-characters">{characteres}</span>
              <Button
                type="submit"
                className="newTweetPage-submit"
                $variant="primary"
                disabled={buttonDisabled}
              >
                {!isFetching ? 'Let`s go!' : 'Sending...'}
              </Button>
            </div>
          </form>
        </div>
      </div>
      {/* Mostramos error */}
      {error && <div className="newTweetPage-error">{error.message}</div>}
    </Content>
  );
};

export default NewTweetPage;
