// componente provider:
import { useState } from 'react';
import { AuthContext } from './context';

const AuthContextProvider = ({ initiallyLogged, children }) => {
  // Creamos state para saber cuando esta logueado el usuario:
  const [isLogged, setIsLogged] = useState(initiallyLogged);
  // Método para hacer login:
  const handleLogin = () => {
    setIsLogged(true);
  };

  // Método para hacer logout:
  const handleLogout = () => {
    setIsLogged(false);
  };

  // Creamos un objeto que pasaremos al value:
  const authValue = {
    isLogged,
    onLogout: handleLogout,
    onLogin: handleLogin,
  };
  return (
    <AuthContext.Provider value={authValue}>{children}</AuthContext.Provider>
  );
};

export default AuthContextProvider;
