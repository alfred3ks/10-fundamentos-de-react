import clsx from 'clsx';
import './TextArea.css';

const TextArea = ({ className, ...props }) => {
  return (
    <div className={clsx('textarea', className)}>
      <textarea
        className="textarea-input"
        name="textarea"
        id="textarea"
        cols="30"
        rows="10"
        {...props}
      ></textarea>
    </div>
  );
};

export default TextArea;
