const Footer = ({ className, children }) => {
  const currentYear = new Date().getFullYear();

  return (
    <footer className={className}>
      @{currentYear} {children}
    </footer>
  );
};

export default Footer;
