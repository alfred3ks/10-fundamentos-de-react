import LoginPage from './pages/auth/LoginPage';
import TweetsPage from './pages/tweets/TweetsPage';
import TweetPage from './pages/tweets/TweetPage';
import NewTweetPage from './pages/tweets/NewTweetPage';
import Layout from './components/layout/Layout';
// import { useAuth } from './pages/auth/context';

// Importamos los componentes de React Router:
import { Routes, Route, Navigate } from 'react-router-dom';
import RequireAuth from './components/RequireAuth';

// const App = () => {
//   const { isLogged } = useAuth();
//   return (
//     <div>
//       {isLogged ? (
//         <Layout>
//           <TweetsPage dark={false} />
//           {/* <NewTweetPage /> */}
//         </Layout>
//       ) : (
//         <LoginPage />
//       )}
//     </div>
//   );
// };

const App = () => {
  return (
    <Routes>
      <Route path="/login" element={<LoginPage />} />
      {/* Rutas anidadas */}
      <Route path="/tweets" element={<Layout />}>
        <Route index element={<TweetsPage />} />
        {/* Ruta dinamica */}
        <Route path=":id" element={<TweetPage />} />
        {/* Protegemos la ruta con nuestro componente */}
        <Route
          path="new"
          element={
            <RequireAuth>
              <NewTweetPage />
            </RequireAuth>
          }
        />
      </Route>

      {/* Redireccion ya que en '/' no tenemos nada */}
      <Route path="/" element={<Navigate to="/tweets" />} />

      {/* con el * le decimos que es cualquier ruta que no hemos tratado */}
      <Route path="*" element={<Navigate to="/404" />} />
      <Route
        path="/404"
        element={
          <Layout>
            <div>404 not fount</div>
          </Layout>
        }
      />
    </Routes>
  );
};

export default App;
