# PropTypes:

Veremos como hacer el tipado de componentes, como usar las props-types. Como sabemos JS es un lenguaje sin tipado estatico, su tipado es dinamico, eso lo podemos conseguir con TypeScript. Pero aqui estamos usando es JavaScript.

Moraleja: debemos aprender TypeScript...

Con los Props Types aseguramos que un componente reciba las varaibles con los tipos que deben ser y asi evitaremos muchos errores, esto es algo que React puso a disposicion de los desarrolladores para poder mejorar y evitar errores de tipos por la caracteristica de JavaScript, si usamos TypeScript esto no es necesario. PropTypes nos ayudara mucho en desarrollo, esto no llega a producción.

La libreria creada por React para las PropTypes es esta:

https://legacy.reactjs.org/docs/typechecking-with-proptypes.html

Esta libreria la debemos instalar:

```bash
npm i prop-types
```

Vamos a ver el componente LikeButton.jsx donde podemos ver como implementar estas propTypes, como vemos nuestro componente recibe por parametros tres valores, uno que un numero, un booleano y una funcion, puede darse el caso que al usar este boton no pasemos alguna de ellas y nuestro boton suba a produccion y ahi se produsca el fallo, sin propTypes salvo que usamos ese boton no veremos el fallo porque el editor no nos dice nada si implementamos propTypes nos aseguramos si o si que pasemos las props y con sus tipos correspondientes:

## pages/tweets/LikeButton.jsx

```javascript
// importamos propTypes:
import PropTypes from 'prop-types';

// Declaramos las propTypes:
LikeButton.propTypes = {
  likes: PropTypes.number.isRequired,
  isLike: PropTypes.bool,
  onLike: PropTypes.func.isRequired,
};
```

Un detalle que vemos que nuestro componente el editor quita ese error que mostraba en las props que recibia el componente.

Ahora ademas de PropTypes por cada componente podemos declarar una propiedad por default, esto es por si por alguna cosa no pasan ese parametro, se le asignamos un valor por efecto, lo vemos con el isLike:

```javascript
// Declaramos los valores por defecto
LikeButton.defaultProps = {
  isLike: false,
};
```

Esto es lo mismo si sobre el propio componente lo declaramos asi: isLike = 'false'

```javascript
const LikeButton = ({ likes, isLike = 'false', onLike }) => {};
```

Se puede hacer de las dos formas.

Lo ideal es empezar con TypeScript... queda pendiente.
