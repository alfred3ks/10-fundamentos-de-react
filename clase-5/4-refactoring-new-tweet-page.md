# Refactoring del componente NewTweetPage.jsx y HeavyComponet.jsx:

Lo que haremos es hacer un refactoring del componente NewTweetPage.jsx para aislar logica y desacoplar un poco los componentes y evitar problemas de performance si tener que usar los hook antes mencionados.

Nos creamos el componente NewTweetRefactoring.jsx y el componente HeavyComponentRefactoring.jsx para separar logica y quitar la memoizacion y ver que muchas veces es mejor pensar separar que aplicar estos hooks.

## NewTweetRefactoring.jsx:

```javascript
import { useMemo, useCallback, useEffect, useRef, useState } from 'react';
import Content from '../../../components/layout/Content';
import Button from '../../../components/shared/Button';
import Photo from '../../../components/shared/Photo';
import Textarea from '../../../components/shared/TextArea';

import { createTweet } from '../service';
import { useNavigate } from 'react-router-dom';

import './NewTweetPage.css';

import HeavyComponentRefactoring from '../../../components/HeavyComponentRefactoring';

// Declaramos las variables para los caracteres del text area
const MIN_CHARACTERES = 5;
const MAX_CHARACTERES = 140;

// 📌 Refactoring formulario:
// eslint-disable-next-line react/prop-types
const NewTweetPageForm = ({ isFetching, onSubmit }) => {
  // Estado para almacenar lo que teclea el usuario:
  const [content, setContent] = useState('');

  // creamos la funcion hangle para el textarea:
  const handleChange = (e) => {
    setContent(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(content);
  };

  // mostramos el maximo de caracteres:
  const characteres = `${content.length} / ${MAX_CHARACTERES}`;

  // desabilitamos el boton para este minimo de caracteres:
  const buttonDisabled = content.length <= MIN_CHARACTERES || isFetching;

  const textareaRef = useRef(null);
  useEffect(() => {
    console.log(textareaRef.current);
    textareaRef.current.focus();
  }, []);

  return (
    <form onSubmit={handleSubmit}>
      <Textarea
        className="newTweetPage-textarea"
        placeholder="Hey! What's up!"
        value={content}
        onChange={handleChange}
        maxLength={MAX_CHARACTERES}
        ref={textareaRef}
      />
      <div className="newTweetPage-footer">
        <span className="newTweetPage-characters">{characteres}</span>
        <Button
          type="submit"
          className="newTweetPage-submit"
          $variant="primary"
          disabled={buttonDisabled}
        >
          {!isFetching ? 'Let`s go!' : 'Sending...'}
        </Button>
      </div>
    </form>
  );
};

// componente NewTweetPage:
const NewTweetPage = () => {
  // Estado para manejar el fetching:
  const [isFetching, setIsFetching] = useState(false);

  // Creamos el estado para el error:
  const [error, setError] = useState(null);

  // Creamos un estado para ver la ref:
  const counterRef = useRef(0);
  const formRef = useRef(null);
  const divRef = useRef(null);

  useEffect(() => {
    counterRef.current++;
    console.log(counterRef.current);
  });
  useEffect(() => {
    console.log(formRef.current);
  }, []);
  useEffect(() => {
    console.log(divRef.current);
  }, []);

  // usamos navigate:
  const navigate = useNavigate();

  // Creamos la funcion para enviar los datos a la api:
  const handleSubmit = async (content) => {
    try {
      setIsFetching(true);
      // Mandamos un objeto content porque es lo que espera la api:
      const tweet = await createTweet({ content });
      setIsFetching(false);
      // Hacemos una navegacion despues de crear el tweet:
      navigate(`../${tweet.id}`, { relative: 'path' });
    } catch (error) {
      if (error.status === '401') {
        navigate('/login');
      } else {
        setIsFetching(false);
        // mostramos un error si no hay conexion con la api:
        setError(error);
        // Establecer un temporizador para limpiar el error después de 5 segundos
        setTimeout(() => {
          setError(null);
        }, 5000);
      }
    }
  };

  // Hooks de memoizacion para fijar la referencia de funciones, objetos, etc:
  const callback = useCallback(() => {}, []);
  const object = useMemo(() => ({}), []);

  return (
    <Content title="What are you thinking?">
      <div
        className="newTweetPage"
        ref={(element) => {
          divRef.current = element;
        }}
      >
        <div className="left">
          <Photo />
        </div>
        <div className="right">
          <NewTweetPageForm isFetching={isFetching} onSubmit={handleSubmit} />
        </div>
      </div>
      {/* Mostramos error */}
      {error && <div className="newTweetPage-error">{error.message}</div>}
      <HeavyComponentRefactoring
        value={37}
        callback={callback}
        object={object}
        arr={[]}
      />
    </Content>
  );
};

export default NewTweetPage;
```

## HeavyComponentRefactoring.jsx:

```javascript
import PropTypes from 'prop-types';

// Funcion de fibonacci para ver memo:
const fibonacci = (n) => {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
};

// Componente que consume la serie de fibonacci:
const HeavyComponentRefactoring = ({ value }) => {
  const result = fibonacci(value);
  return (
    <div>
      Fibonacci({value}) = <code>{result}</code>
    </div>
  );
};

// PropTypes:
HeavyComponentRefactoring.propTypes = {
  value: PropTypes.number.isRequired,
};

export default HeavyComponentRefactoring;
```
