import Footer from './Footer';
import Header from './Header';

import { Outlet } from 'react-router-dom';

import './Layout.css';

// const Layout = ({ children }) => {
//   return (
//     <div className="layout">
//       <Header className="layout-header bordered" />
//       <main className="layout-main bordered">{children}</main>
//       <Footer className="layout-footer bordered">KeepCoding</Footer>
//     </div>
//   );
// };

// Rutas anidadas: Renderizamos el componente Outles de React Router
const Layout = ({ children }) => {
  return (
    <div className="layout">
      <Header className="layout-header bordered" />
      <main className="layout-main bordered">
        {children ? children : <Outlet />}
      </main>
      <Footer className="layout-footer bordered">KeepCoding</Footer>
    </div>
  );
};

export default Layout;
