import clsx from 'clsx';
import Icon from '../../shared/Icon';
import AuthButton from '../../../pages/auth/AuthButton';

import { Link, NavLink } from 'react-router-dom';

import './Header.css';

const Header = ({ className }) => {
  return (
    <header className={clsx('header', className)}>
      <div className="header-logo">
        <Link to={'/'}>
          <Icon width={32} fill="crimson" />
        </Link>
      </div>

      <nav className="header-nav">
        <NavLink to={'/tweets/new'}>Create tweet</NavLink>|
        <NavLink to={'/tweets'} end>
          Lastest tweets
        </NavLink>
        <AuthButton className={'header-button'} />
      </nav>
    </header>
  );
};

export default Header;
