import clsx from 'clsx';
import defaultPhoto from '../../../assets/images/default-profile.png';
import './Photo.css';

const Photo = ({ className, ...props }) => {
  return (
    <img
      className={clsx('photo', className)}
      src={defaultPhoto}
      alt={'default profile'}
      {...props}
    />
  );
};

export default Photo;
