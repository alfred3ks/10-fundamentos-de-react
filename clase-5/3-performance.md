# Performance:

Un componente de React renderiza cada vez que:

- Cambia algun estado del componente,

- Cambia algún valor en un contexto consumido por el componente,

- Renderiza su padre, independientemente de que cambien las props que este envie, cambie estas o no.

Esto ultimo puede hacer que un componente renderice innecesariamente para devolver el mismo resultado que el render anterio. Normalmente esto no es un problema par React, pero en aplicaciones mas grandes puede provocar problemas de performance.

<span style="color: #ff5733;">NOTA:</span> Preocuparse de los render cuando realmente sea un problema.

Vamos a crear un cálculo que sea potente en un componente ya que nuestra app no tiene ningun componente que haga esto para poder ver como aplicar como mejorar ese componente para que no afecte a la aplicación.

Para trabajar el performance de nuestra aplicación cuando sea necesario tenemos unos hooks que nos proporciona React.

## Tenemos el componente memo: HOC

Este componente de orden superior (HOC) permite evitar el render de un componente cuando sus props no han cambiado. Memo no evita renders por cambio de estado o contexto.
memo se utiliza para memorizar (almacenar en caché) el resultado de un componente funcional, evitando su renderización innecesaria si las props no han cambiado. Esto puede ser útil para mejorar el rendimiento al evitar el procesamiento y la renderización redundante.

Veamos un ejemplo en nuestra aplicación. Lo seguimos haciendo en nuestro componente NewTweetPage.jsx, crearemos un componente que calcule la serie de fibonacci, lo llamaremos HeavyComponent. El componente lo crearemos en la carpeta component y lo ejecutaremos en NewTweetPage.jsx:

## Fibonacci.jsx:

```javascript
import PropTypes from 'prop-types';

// Funcion de fibonacci para ver memo:
const fibonacci = (n) => {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
};

// Componente que consume la serie de fibonacci:
const HeavyComponent = ({ value }) => {
  const result = fibonacci(value);
  return (
    <div>
      Fibonacci({value}): <code>{result}</code>
    </div>
  );
};

// PropTypes:
HeavyComponent.propTypes = {
  value: PropTypes.number.isRequired,
};

export default HeavyComponent;
```

## NewTweetsPage.jsx:

```javascript
import HeavyComponent from '../../../components/HeavyComponent';

<HeavyComponent value={37} />;
```

Como vemos cada vez que renderiza el componente este va muy lento, esto es porque el componente HeavyComponent relentiza el render, Lo vemos cada vez que metemos una letra en el input, aqui vemos un problema de performance. Para ver que componente es el que esta causando ese problema de performance tenemos en la consola de desarrollador el profiler podemos iniciar una grabación y ver cual es que en tiempo tarda mas en hacer el render.

![](./img/performance.png)

Aqui vemos cual es el componente que introduce mas tiempo de render. Lo vamos a solucionar usando la funcion memo() de React.

## Fibonacci.jsx:

Usamos la funcion memo() de react y luego exportamos ese nuevo componente:

```javascript
import PropTypes from 'prop-types';
import { memo } from 'react';

// Funcion de fibonacci para ver memo:
const fibonacci = (n) => {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
};

// Componente que consume la serie de fibonacci:
const HeavyComponent = ({ value }) => {
  const result = fibonacci(value);
  return (
    <div>
      Fibonacci({value}) = <code>{result}</code>
    </div>
  );
};

// Solucionamos problema de performance usando memo:
const MemoHeavyComponent = memo(HeavyComponent);

// PropTypes:
HeavyComponent.propTypes = {
  value: PropTypes.number.isRequired,
};

export default MemoHeavyComponent;
```

## NewTweetsPage.jsx:

```javascript
import MemoHeavyComponent from '../../../components/HeavyComponent';

<MemoHeavyComponent value={37} />;
```

Volvemos a probar y vemos que ya no se ve afectado el componente del input por el de Fibonacci. Lo vemos en el inspector:

![](./img/performance-1.png)

Aqui lo que vemos que durante el render no se ejecuta el render de Fibonacci, por lo tanto no impacta en el componente NewTweetPage.jsx. Esto es la memoizacion de un componente.

La funcion memo() permite pasar un segundo parametro, que seran una funcion que recibe las props anteriores y las props nuevas y en funcion de eso le diremos que renderice o no. Es un ajuste mas fino que podemos dar.

## Fibonacci.jsx:

```javascript
// Solucionamos problema de performance usando memo:
const MemoHeavyComponent = memo(HeavyComponent, (prevProps, nextProps) => {
  console.log(nextProps, prevProps);
  return prevProps === nextProps;
});
```

A mi aqui asi no me memoiza. No se porque.

Ahora vamos a ver que pasaria si el padre le pasa otra props, una props en forma de función.

## NewTweetsPage.jsx:

```javascript
<MemoHeavyComponent value={37} callback={() => {}} />
```

Para este caso perdemos la memoizacion porque las funciones se guardan por referencia y esta cambia en cada render. Se crea una funcion nueva. Entonces entiende que las props cambia y entonces vuelve a renderizar el componente Fibonacci.jsx.
Esto tambien pasara si pasamos un arreglo {}, o un array []. Para esto tendriamos que ajustar nuestra funcion de memoizacion para que solo tenga en cuenta el value asi:

## Fibonacci.jsx:

```javascript
// Solucionamos problema de performance usando memo:
const MemoHeavyComponent = memo(HeavyComponent, (prevProps, nextProps) => {
  console.log(nextProps, prevProps);
  return prevProps.value !== nextProps.value;
});
```

Y ahora si, vemos como si memoiza nuestro componente.

## Otra solucion, hook useCallback():

Vamos a ver otra manera de arreglar esto, lo de pasarle funciones. desde el padre fijar la referencia, para eso tenemos un hook que es useCallback(). Pasamos como primer parametro la funcion, objeto, arreglo, y como segundo un arreglo de dependencias. Esa funcion que pasamos se generara cada vez que cambie el valor de la dependencia. si la pasamos vacia vemos que la funcion mantiene la referencia, por lo tanto ahora ya funciona la memoización.

## Una funcion ()=>{}:

## Fibonacci.jsx:

```javascript
// Solucionamos problema de performance usando memo:
const MemoHeavyComponent = memo(HeavyComponent);
```

## NewTweetsPage.jsx:

```javascript
import { useCallback } from 'react';
import MemoHeavyComponent from '../../../components/HeavyComponent';

const callback = useCallback(() => {}, []);

<MemoHeavyComponent value={37} callback={callback} />;
```

## Un objeto {}:

Ahora vamos a pasarle un objeto a nuestro componente, volvemos a perder la memoización:

## NewTweetsPage.jsx:

```javascript
import { useCallback } from 'react';
import MemoHeavyComponent from '../../../components/HeavyComponent';

const callback = useCallback(() => {}, []);

<MemoHeavyComponent value={37} callback={callback} object={{}} />;
```

Tambien lo resolvemos desde el padre usando useMemo(), otro hook:

## NewTweetsPage.jsx:

```javascript
import { useMemo } from 'react';
import MemoHeavyComponent from '../../../components/HeavyComponent';

// Hooks de memoizacion para fijar la referencia de funciones, objetos, etc:
const object = useMemo(() => ({}), []);

<MemoHeavyComponent value={37} callback={callback} object={object} />;
```

Lo probamos y vemos que volvemos a tener en funcionamiento la memoizacion. Como vemos usamos useCallback() para funciones y useMemo() para cualquier otro valor, arreglos o objetos.

Este es uno de los usos que podemos hacer con useMemo(), lo usamos para mantener referencias de objetos, arreglos, tambien lo podemos usar para evitar calculos. Lo podemos ver en el componente Fibonacci.jsx: Pasamos un arreglo tambien:

## NewTweetsPage.jsx:

```javascript
<MemoHeavyComponent value={37} callback={callback} object={object} arr={[]} />
```

## Fibonacci.jsx:

```javascript
import { useMemo } from 'react';

// Componente que consume la serie de fibonacci:
export const HeavyComponent = ({ value }) => {
  const result = useMemo(() => {
    fibonacci(value);
  }, [value]);
  return (
    <div>
      Fibonacci({value}) = <code>{result}</code>
    </div>
  );
};
```

```javascript
import { HeavyComponent } from '../../../components/HeavyComponent';

// Hooks de memoizacion para fijar la referencia de funciones, objetos, etc:
const callback = useCallback(() => {}, []);
const object = useMemo(() => ({}), []);

<HeavyComponent value={37} callback={callback} object={object} arr={[]} />;
```

Aqui vemos que la funcion fibonacci(value), es la que relentiza el tema, con useMemo(), solo ejecutaremos esa funcion cuando el value cambie, asi ya memoizamos la funcion. Con esto estamos es evitando el calculo de la funcion no el render del componente.

Ahora que ya conocemos useMemo() y hemos visto que en donde se ejecuta la funcion fibonacci(value) que es el punto critico ya no haria falta usar memo(), importaria el componente HeavyComponent y tan felices. Estas son las herramientas. Estas herramientas se usan cuando hay un problema no por defecto.
