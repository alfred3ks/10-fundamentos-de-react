# Hook useRef():

Refs:

Valores que los componentes recuerdan pero sin provocar render, es como un estado (useState) pero sin provocar render al cambiar.

Se define con el hook useRef y permite guardar:

- timeout o interval ids.
- referencias a elementos del DOM.
- cualquier valor que necesitemos guardar en un componente y que no queramos que cause render.

Vamos a ver como usar el hook useRef(), lo haremos en el componente NewTweetPage.jsx:

Lo que haremos es contar las veces que se renderiza el componente, imaginemos que lo hacemos con estado, tendria que ser asi:

## NewTweetPage.jsx:

```javascript
import { useState } from 'react';
// Creamos un estado para ver la ref:
const [counter] = useState({ current: 0 });

// Ref con un state:
useEffect(() => {
  counter.current = counter.current + 1;
  console.log(counter.current);
});
```

Con esto lo tendriamos, contariamos las veces que se renderiza el componente usando un el hook useState(), pero como vemos asi no se usa useState(), para eso React creado un hook para este tipos de cosas que deseamos hacer sin tener que formar un render adicional y no usar useState(), es el hook useRef():

## NewTweetPage.jsx:

```javascript
import { useRef } from 'react';
// Creamos un estado para ver la ref:
const counterRef = useRef(0);

useEffect(() => {
  counterRef.current++;
  console.log(counterRef.current);
});
```

De esta manera estamos haciendo lo mismo que antes pero usando el hook de ref, que no actualiza el estado del componente. Osea no impacta sobre el componente, esta haciendo un calculo independiente al componente sin impactar en el, sin provocar un render. No es muy usado pero es bueno saber que existe.

## Acceder a elemento del DOM:

Aparte de esto que hemos visto las refs tienen otro uso, para acceder a elementos del DOM.

Normalmente podemos acceder a un elemento del DOM usando document.getElementById(), le pasamos el elemento que deseemos. En react esto no lo hacemos asi, es algo raro acceder a elementos asi, pero si necesitamos acceder React lo facilita con refs. Vamos a ver como hacerlo en el mismo componente:

```javascript
const formRef = useRef(null);

useEffect(() => {
  console.log(formRef.current);
}, []);

<form onSubmit={handleSubmit} ref={formRef}></form>;
```

Asi vemos que nos muestra por consola el elemento html. Ya podemos acceder a el elemento del DOM y hacer lo que necesitemos.

## Otra forma de obtener un elemento con ref:

Existe otra manera de obtener un elemento usando ref. Pasandole una funcion. Lo normal es hacerlo como antes. Pero debemos saber que tambien se puede usar asi.

```javascript
const divRef = useRef(null);

useEffect(() => {
  console.log(divRef.current);
}, []);

<div
  className="newTweetPage"
  ref={(element) => {
    divRef.current = element;
  }}
></div>;
```

Vemos como accedemos a los div.

## Como usar Refs a componentes propios, componentes de tipo funcion:

Si tratamos de pasar una refs a un componentes de tipo funcion React nos daria un warning y lo diria que no podemos usar una refs a un componente de tipo funcion. Osea como lo hemos visto no podriamos hacerlo. Solo podemos hacrlo para elementos nativos. Lo podemos hacer con un par de tecnicas.

## 1 - Crearnos una propiedad nuestra propia, cualquier nombre, y recibirla por props.

## NewTweetPage.jsx:

```javascript
const textareaRef = useRef(null);

useEffect(() => {
  console.log(textareaRef.current);
  textareaRef.current.focus();
}, []);

<Textarea
  className="newTweetPage-textarea"
  placeholder="Hey! What's up!"
  value={content}
  onChange={handleChange}
  maxLength={MAX_CHARACTERES}
  innerRef={textareaRef}
/>;
```

## TextArea.jsx:

```javascript
const TextArea = ({ className, innerRef, ...props }) => {
  return (
    <div className={clsx('textarea', className)}>
      <textarea
        className="textarea-input"
        name="textarea"
        id="textarea"
        cols="30"
        rows="10"
        ref={innerRef}
        {...props}
      ></textarea>
    </div>
  );
};
```

Vemos como tenemos acceso al elemento.

## 2 - Tenemos la siguiente opción que es como lo recomienda react. Usando la propiedad forwardRef de React.

Envolvemos el componente en una llamada a la funcion forwarRef(), el primer parametro son las props, y el segundo las ref:

## TextArea.jsx:

```javascript
import { forwardRef } from 'react';

const TextArea = forwardRef(({ className, ...props }, ref) => {
  return (
    <div className={clsx('textarea', className)}>
      <textarea
        className="textarea-input"
        name="textarea"
        id="textarea"
        cols="30"
        rows="10"
        ref={ref}
        {...props}
      ></textarea>
    </div>
  );
});
```

## NewTweetPage.jsx: Pasamos la ref como cualquier elemento nativo del DOM:

```javascript
const textareaRef = useRef(null);

useEffect(() => {
  console.log(textareaRef.current);
  textareaRef.current.focus();
}, []);

<Textarea
  className="newTweetPage-textarea"
  placeholder="Hey! What's up!"
  value={content}
  onChange={handleChange}
  maxLength={MAX_CHARACTERES}
  ref={textareaRef}
/>;
```

OJO, los componentes creados con styled component ya permiten pasar las refs como cuanquier elemento nativo. Esto es debido que la libreria ya envuelve ese componente de forma interna en un forwardRef(), asi ya podemos mpasar el ref={}.
